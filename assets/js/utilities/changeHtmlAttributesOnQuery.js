define(["exports", "module", "jquery"], function (exports, module, _jquery) {
  "use strict";

  var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();

  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

  /**
  * ChangeHtmlAttributesOnQuery.js - changes attributes on media query
  *
  * @since 1.1.12b
  *
  * @author Digital Services <communications@det.nsw.edu.au>
  * @copyright © 2016 State Government of NSW 2016
  *
  * @class
  * @requires jQuery
  */

  var ChangeHtmlAttributesOnQuery = (function () {

    /**
     *
     * @param {jquery} selector - the element which is modified
     * @param {string} config.media_query - the media query which when triggers removes the tabindex attribute
     * @param {jquery} config.attributes_to_remove - the attributes to remove on the selector, separated by a space
     * @param {object} config.attribute_changes - the attibutes to change on the selector in "name" : "value" pairs
     */

    function ChangeHtmlAttributesOnQuery(selector, config) {
      _classCallCheck(this, ChangeHtmlAttributesOnQuery);

      this.selector = selector;

      this.config = {
        media_query: "(min-width: 960px)",
        attributes_to_remove: "",
        attribute_changes: ""
      };

      // Check if config has been passed to constructor
      if (config) {
        // Merge default config with passed config
        this.config = _jquery.extend(true, {}, this.config, config);
      }

      // set the mediq query
      var media_query = window.matchMedia(this.config.media_query);
      // Test the media query
      this.testMediaQuery(media_query, selector, config);
    }

    /**
     * Tests the media query
     *
     * @param {string} mq - the media query string
     */

    _createClass(ChangeHtmlAttributesOnQuery, [{
      key: "testMediaQuery",
      value: function testMediaQuery(mq, selector, config) {
        if (mq.matches) {
          this.removeAttributes(selector, config.attributes_to_remove);
          this.attributeChanges(selector, config.attribute_changes);
        }
      }

      /**
       * Removes attributes from selector
       *
       * @param {jquery} selector - the element which is modified
       * @param {jquery} removals - the attributes to remove on the selector separated by a space
       */
    }, {
      key: "removeAttributes",
      value: function removeAttributes(selector, removals) {
        (0, _jquery)(selector).removeAttr(removals);
      }

      /**
       * Changes attributes on selector
       *
       * @param {jquery} selector - the element which is modified
       * @param {object} changes - the attibutes to change on the selector in "name" : "value" pairs
       */
    }, {
      key: "attributeChanges",
      value: function attributeChanges(selector, changes) {
        (0, _jquery)(selector).attr(changes);
      }
    }]);

    return ChangeHtmlAttributesOnQuery;
  })();

  module.exports = ChangeHtmlAttributesOnQuery;
});