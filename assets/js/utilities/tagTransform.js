define(["exports", "module", "jquery"], function (exports, module, _jquery) {
  "use strict";

  var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();

  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

  /**
   *
   *
   * @since 1.0.0
   *
   * @author Digital Services <communications@det.nsw.edu.au>
   * @copyright © 2015 State Government of NSW 2015
   *
   * @class
   * @requires jQuery
   */

  var TagTransform = (function () {

    /**
     * Creates a new Tag Transform
     *
     * @constructor
     *
     * @param {String|Element|jQuery} selector - Either a CSS selector, DOM element or matched jQuery object
     * @param {Object} config - class configuration options. Options vary depending on need
     * @param {String} config.selectors.active - class selector for the active state of toggle buttons
     *
     */

    function TagTransform(selector, config) {
      _classCallCheck(this, TagTransform);

      // Default class config options
      this.config = {};

      // Check if config has been passed to constructor
      if (config) {
        // Merge default config with passed config
        this.config = _jquery.extend(true, {}, this.config, config);
      }

      // Check if selector has been passed to constructor
      if (selector) {
        // Use jQuery to match find the relevant DOM element(s)
        this.$selector = (0, _jquery)(selector);
        // News article tags
        this.$tags = this.$selector.data("gef-tags").split(";");
        this.$tagAmount = this.$tags.length;
        // News article URL
        this.$newsIndex = this.$selector.data("news-index");
        // Empty UL
        this.$list = (0, _jquery)("<ul class='gef-tag-list__list'></ul>");
        // Transform tags into link list
        this.tagTransform();
        // Add list to article
        this.$selector.replaceWith(this.$list);
      }
    }

    /**
    * Exports the Class Toggle class as a module
    * @module
    */

    _createClass(TagTransform, [{
      key: "tagTransform",
      value: function tagTransform() {
        //Split tags
        for (var i = 0; i < this.$tagAmount; i++) {
          var currentTag = this.$tags[i];
          currentTag = currentTag.charAt(0) == " " ? currentTag.substring(1) : currentTag;
          var tagElement = "<li>&nbsp;<a href='" + this.$newsIndex + "?tag=" + currentTag + "' class='gef-smaller-text'>" + currentTag + "</a></li>";
          this.$list.append((0, _jquery)(tagElement));
        }

        return this.$list;
      }
    }]);

    return TagTransform;
  })();

  module.exports = TagTransform;
});