define(["exports", "module", "jquery"], function (exports, module, _jquery) {
  "use strict";

  var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();

  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

  /**
   * Sends events to Google Analytics for precise statistics for downloaded filetypes
   *
   * @since 2.0.2
   *
   * @author Digital Services <communications@det.nsw.edu.au>
   * @copyright © 2015 State Government of NSW 2015
   *
   * @class
   * @requires jQuery
   */

  var GaEventTracker = (function () {

    /**
    * Creates a new AnchorBox
    *
    * @constructor
    *
    * @param {Object}   fileTypes - the extensions and their category to be referenced by GA
    *
    */

    function GaEventTracker(selector, config) {
      _classCallCheck(this, GaEventTracker);

      this.$selector = (0, _jquery)(selector);

      this.config = {

        events: {
          downloads: {
            types: {
              "Image": ['gif', 'jpg', 'jpeg', 'png'],
              "Word": ['doc', 'docx', 'dotx', 'dot', 'dotm', 'docm', 'rtf'],
              "PDF": ['pdf'],
              "Document": ['rotx', 'pub', 'txt', 'ppt', 'pptx', 'pps', 'pptm', 'xps', 'xls', 'xlsx', 'xlsm', 'xml', 'xlsb'],
              "Video": ['flv', 'wmv', 'avi', 'mkv', 'mp4'],
              "Audio": ['mp3', 'ogg', 'wav', 'wma']
            }
          }
        }
      };

      // Check if config has been passed to constructor
      if (config) {
        // Merge default config with passed config
        this.config = _jquery.extend(true, {}, this.config, config);
      }

      // Run the apply download event functions
      if (this.config.events.downloads) {
        this.applyDownloadEvent();
      }
    }

    /**
    * Loops through all the different types of download and set approrpiate events
    *
    * @applyDownloadEvent
    *
    */

    _createClass(GaEventTracker, [{
      key: "applyDownloadEvent",
      value: function applyDownloadEvent() {

        var fileTypes = this.config.events.downloads.types;

        for (var fileCategory in fileTypes) {
          var extentions = fileTypes[fileCategory],
              extensionsArr = [];

          for (var extentionIndex in extentions) {
            extensionsArr[extentionIndex] = "\\." + extentions[extentionIndex];
          }

          var re = new RegExp("(" + extensionsArr.join("|") + ")$", "i");

          this.$selector.filter(function () {
            return re.test((0, _jquery)(this).attr('href'));
          }).each(function (a) {
            (0, _jquery)(this).attr("link-category", fileCategory);
          }).bind("click", function (e) {
            ga('send', 'event', 'File download', (0, _jquery)(this).attr("link-category"), (0, _jquery)(this).attr("href"));
          }).bind("contextmenu", function (e) {
            ga('send', 'event', 'File download', (0, _jquery)(this).attr("link-category"), (0, _jquery)(this).attr("href"));
          });
        }
      }
    }]);

    return GaEventTracker;
  })();

  module.exports = GaEventTracker;
});