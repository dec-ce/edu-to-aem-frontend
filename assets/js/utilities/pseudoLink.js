define(["exports", "module", "jquery", "core/notificationCentre"], function (exports, module, _jquery, _coreNotificationCentre) {
  "use strict";

  var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();

  function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

  var _NotificationCentre = _interopRequireDefault(_coreNotificationCentre);

  /**
   * Create a new PseudoLink
   *
   * @example
   * // Instantiate and check/tag the passed link using class constructor
   * let pseudoLink = new PseudoLink("[data-pseudo-link]", { selectors: { container: "[data-pseudo-link]" }})
   *
   * @class
   * @requires jQuery
   */

  var PseudoLink = (function () {

    /**
     * @param {(String|jQuery)} selector - A CSS selector or jQuery object
     * @param {string} config.classes.container - a class which is added to the selector element
     *
     * @returns {LinkTagger} Returns a PseudoLink object
     */

    function PseudoLink(selector, config) {
      _classCallCheck(this, PseudoLink);

      this.config = {
        classes: {
          container: "gef-pseudo-link"
        },
        selectors: {
          anchors: "a"
        }
      };

      // Check if config has been passed to constructor
      if (config) {
        // Merge default config with passed config
        _jquery.extend(this.config, config);
      }

      // Check if selector has been passed to constructor
      if (selector) {
        var $container = (0, _jquery)(selector);
        var localConfig = $container.attr("data-pseudo-link");

        if (localConfig != "data-pseudo-link" && localConfig != false) {
          this.config.selectors.anchors = localConfig;
        }

        // Use jQuery to find the relevant selector in the DOM
        this.addLinks(selector);
      }

      var notificationCentre = _NotificationCentre["default"].shared();
      notificationCentre.publish("pseudoLinkInit");
    }

    /**
     *
     *
     * @param {Sring|jQuery|Element} selector - A CSS selector or DOM element to turn into a "link"
     *
     * @returns {jQuery|false} Returns the result of a jQuery match however if zero elements are match then false will be returned
     */

    _createClass(PseudoLink, [{
      key: "addLinks",
      value: function addLinks(selector) {
        var $containers = (0, _jquery)(selector);
        var _self = this;

        if ($containers.length !== 0) {
          $containers.each(function (index, container) {
            _self.addLink(container);
          });

          return $containers;
        } else {
          return false;
        }
      }

      /**
       * Turns a container element (e.g. div) that contains an anchor (immediate child) into clickable element that links to the same url as it's child anchor
       *
       * @param {Element} container - A DOM element containing an anchor tag
       *
       * @returns jQuery The element encapsulated in a jQuery object
       */
    }, {
      key: "addLink",
      value: function addLink(container) {
        var _this = this;

        var $container = (0, _jquery)(container);
        var anchor = $container.find(this.config.selectors.anchors);

        // Check if there is 1 or more anchors in the container
        if (anchor.length > 0) {
          (function () {
            // Grab the first anchors
            var $anchor = (0, _jquery)(anchor[0]);
            var url = $anchor.attr("href");
            var target = $anchor.attr("target");

            $container.addClass(_this.config.classes.container);

            $container.on("click", function () {
              window.open(url, target ? target : "_self");
            });
          })();
        }

        return $container;
      }
    }]);

    return PseudoLink;
  })();

  module.exports = PseudoLink;
});