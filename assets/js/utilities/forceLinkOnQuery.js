define(["exports", "module", "jquery"], function (exports, module, _jquery) {
  "use strict";

  var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();

  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

  /**
  * forceLinkOnQuery.js - utility which forces linking when triggered for link elements at specified breakpoints where they have previously been blocked via preventDefault()
  *
  * @since 1.1.12
  *
  * @author Digital Services <communications@det.nsw.edu.au>
  * @copyright © 2016 State Government of NSW 2016
  *
  * @class
  * @requires jQuery
  */

  var ForceLinkOnQuery = (function () {

    /**
     *
     * @param {jquery} selector - the element which is modified
     * @param {string} config.media_query - the media query which when triggers forces linking
     *
     */

    function ForceLinkOnQuery(selector, config) {
      _classCallCheck(this, ForceLinkOnQuery);

      this.selector = selector;

      this.config = {
        media_query: "(min-width: 960px)"
      };

      // Check if config has been passed to constructor
      if (config) {
        // Merge default config with passed config
        this.config = _jquery.extend(true, {}, this.config, config);
      }

      // set the mediq query
      var media_query = window.matchMedia(this.config.media_query);
      // Test the media query
      this.testMediaQuery(media_query, selector);
    }

    /**
     * Tests the media query
     *
     * @param {string} mq - the media query string
     */

    _createClass(ForceLinkOnQuery, [{
      key: "testMediaQuery",
      value: function testMediaQuery(mq, selector) {
        if (mq.matches) {
          this.forceLink(selector);
        }
      }

      /**
       * Forces linking on click
       *
       * @param {jquery} selector - the element in question
       */
    }, {
      key: "forceLink",
      value: function forceLink(selector) {

        // check selector is an anchor tag or find the closest one
        if (!(0, _jquery)(selector).is('a')) {
          selector = (0, _jquery)(selector).closest('a');
        }

        (0, _jquery)(selector).on("click", function () {
          window.location = (0, _jquery)(this).attr('href');
        });
      }
    }]);

    return ForceLinkOnQuery;
  })();

  module.exports = ForceLinkOnQuery;
});