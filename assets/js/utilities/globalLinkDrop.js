define(["exports", "module", "jquery"], function (exports, module, _jquery) {
    "use strict";

    function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

    /**
     * globalLinkDrop.js Binds a custom link to the JIRA collection dialogue.
     *
     * @since 1.0.0
     *
     * @author Digital Services <communications@det.nsw.edu.au>
     * @copyright © 2015 State Government of NSW 2015
     *
     * @class
     * @requires jQuery
     */

    var globalLinkDrop =

    /**
     * Generates the dropdown on button click
     *
     * @constructor
     *
     * @param {String|Element|jQuery} selector - Either a CSS selector, DOM element or matched jQuery object
     * @param {Object} config - class configuration options. Options vary depending on need
     * @param {String} config.classes.dropdown - class name of the generated dropdown
     *
     */

    function globalLinkDrop(selector, config) {
        _classCallCheck(this, globalLinkDrop);

        this.config = {
            classes: {
                dropdown: "gef-global-links-dropdown"
            }
        };

        // Check if config has been passed to constructor
        if (config) {
            // Merge default config with passed config
            this.config = _jquery.extend(true, {}, this.config, config);
        }
        var dropDownClass = this.config.classes.dropdown;

        //The login button click event triggers the dropdown
        (0, _jquery)(selector).on("click", function (e) {
            e.stopPropagation();
            (0, _jquery)("." + dropDownClass).toggle();
            (0, _jquery)(selector).toggleClass('rotate');
            if ((0, _jquery)(selector).hasClass('rotate')) {
                (0, _jquery)(selector).attr("aria-expanded", "true");
            } else {
                (0, _jquery)(selector).attr("aria-expanded", "false");
            }
        });

        (0, _jquery)(document).on('click', function (e) {
            (0, _jquery)("." + dropDownClass).hide();
            if ((0, _jquery)(selector).hasClass('rotate')) {
                (0, _jquery)(selector).toggleClass('rotate');
                (0, _jquery)(selector).attr("aria-expanded", "false");
            }
        });

        //Hide the drop down after tabbing through the last list item
        (0, _jquery)("." + dropDownClass + " li:last-child ul li:last-child").on('keydown', function (e) {
            var code = e.keyCode ? e.keyCode : e.which;

            if (code == 9 && e.shiftKey) {//keycode 9 is for tab
                // do nothing
            } else if (code === 9) {
                    (0, _jquery)("." + dropDownClass).hide();
                    if ((0, _jquery)(selector).hasClass('rotate')) {
                        (0, _jquery)(selector).toggleClass('rotate');
                        (0, _jquery)(selector).attr("aria-expanded", "false");
                    }
                }
        });
    };

    module.exports = globalLinkDrop;
});