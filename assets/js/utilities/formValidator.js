define(["exports", "module", "jquery", "vendor/jquery-validation/validate"], function (exports, module, _jquery, _vendorJqueryValidationValidate) {
  "use strict";

  function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

  var _Validate = _interopRequireDefault(_vendorJqueryValidationValidate);

  //import moment from "vendor/moment/moment"

  /**
   *
   * @since 1.0.0
   *
   * @author Digital Services <communications@det.nsw.edu.au>
   * @copyright © 2018 State Government of NSW 2018
   *
   * @class
   * @requires jQuery
   */

  var formValidator = function formValidator(selector) {
    _classCallCheck(this, formValidator);

    var $form = (0, _jquery)(selector);

    $form.validate();

    // Validate if the user has entered the dot after @-- (--@--.--)
    _jquery.validator.addMethod("emailfull", function (value, element) {
      return this.optional(element) || /^.+@.+\..+$/.test(value);
    }, "Your email address must be in the format of name@domain.com");

    // Validate if the user has entered an AU phone. (<input type="text" ...)
    _jquery.validator.addMethod("phoneAU", function (phone_number, element) {
      phone_number = phone_number.replace(/\s+/g, "");
      var phoneExpression = /^\({0,1}((0|\+61)(2|4|3|7|8)){0,1}\){0,1}( |-){0,1}[0-9]{2}( |-){0,1}[0-9]{2}( |-){0,1}[0-9]{1}( |-){0,1}[0-9]{3}$/;
      return this.optional(element) || phone_number.match(phoneExpression);
    }, "Please specify a valid phone number");

    // Validates the textbox based on the select option
    _jquery.validator.addMethod('validSelectTextbox', function (value, element) {

      var optionSelected = (0, _jquery)(".select-type-required").val(); //console.log("deep " + optionSelected)           
      var textBoxClass = (0, _jquery)(element).attr('class');

      var mytextBoxClass = textBoxClass.replace(/\s{2,}/g, ' ').split(' ');

      var className = optionSelected + "-type-required";
      var classRequired = "." + className;

      var validationRequired = undefined;
      var optionValue = undefined;
      _jquery.each(mytextBoxClass, function (index, value) {
        if (value === className) {
          // console.log("If Give className - "+className+"Value = "+ value)
          optionValue = value.replace(/\s{2,}/g, ' ').split('-'); // console.log("If Give option - "+optionValue[0])
          validationRequired = "Yes";
        }
      });

      if (validationRequired == "Yes") {
        return optionSelected != optionValue[0] || optionSelected === optionValue[0] && (0, _jquery)(classRequired).val() != "";
      } else {
        return true;
      }
    }, "This field is required ");

    // Validates the textbox for letters only ie no numbers or funny characters allowed
    _jquery.validator.addMethod("lettersOnly", function (value, element) {
      return this.optional(element) || value == value.match(/^[a-zA-Z\s]*$/);
    });

    // Validates the textbox for any HTML tags ie. <--->
    _jquery.validator.addMethod("validTextNoTags", function (value, element) {
      var reg = /<(.|\n)*?>/g;
      return this.optional(element) || reg.test(value) == false;
    }, "No HTML tags allowed");

    // Validates date in the format DD/MM/YYYY
    _jquery.validator.addMethod("australianDate", function (value, element) {
      return value.match(/^\d\d?\/\d\d?\/\d\d\d\d$/);
    }, "Please enter a valid date in the format dd/mm/yyyy.");

    // $.validator.addMethod("validDateSlash", function(value, element) {
    //     return this.optional(element) || moment(value,"DD/MM/YYYY").isValid()
    // }, "Please enter a valid date in the format DD/MM/YYYY");
  };

  module.exports = formValidator;
});