define(["exports", "module", "jquery", "keycodes", "uikit", "vendor/annyang/annyang"], function (exports, module, _jquery, _keycodes, _uikit, _vendorAnnyangAnnyang) {
  "use strict";

  var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();

  function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

  var _KeyCodes = _interopRequireDefault(_keycodes);

  var _UI = _interopRequireDefault(_uikit);

  /**
   * Creates a new HeyGEF. Inserts a language specific HeyGEF at the beginning of a matched element. The language is detected using the HTML lang attribute
   *
   * @since 1.0.0
   *
   * @author Digital Services <communications@det.nsw.edu.au>
   * @copyright © 2015 State Government of NSW 2015
   *
   * @class
   * @requires jQuery
   */

  var HeyGEF = (function () {

    /**
     * Creates a new HeyGEF
     *
     * @constructor
     *
     * @param {String|Element|jQuery} selector - Either a CSS selector, DOM element or matched jQuery object
     * @param {Object} config - class configuration options. Options vary depending on need
     * @param {Object} config.HeyGEF - A plain Object containing 1 or more key/value pairs where the language code is used as the key and the HeyGEF is the value
     * @param {String} config.language - the default language code to use when lang attribute is not present or a greetign for the lang code is not found
     *
     * @example
     * // Instantiate a new HeyGEF
     * let HeyGEF = new HeyGEF(".gef-main-content .lead", { HeyGEF: { de: "Hallo" } })
     */

    function HeyGEF(selector, config) {
      var _this = this;

      _classCallCheck(this, HeyGEF);

      // Check if only one argument has been passed
      if (arguments.length === 1) {
        // If argument[0] is a config object then set the config arg and nullify the selector arg
        if (selector instanceof Object && !(selector instanceof _jquery)) {
          config = selector;
          selector = null;
        }
      }

      // Default class config options
      this.config = {
        commands: {
          "search for *query": this.search.bind(this)
        },
        keyCommand: [_KeyCodes["default"].upArrow, _KeyCodes["default"].upArrow, _KeyCodes["default"].downArrow, _KeyCodes["default"].downArrow, _KeyCodes["default"].leftArrow, _KeyCodes["default"].rightArrow, _KeyCodes["default"].leftArrow, _KeyCodes["default"].rightArrow, _KeyCodes["default"].b, _KeyCodes["default"].a],
        searchURI: "http://search.det.nsw.edu.au/search?client=search_dec_nsw_gov_au_frontend&output=xml_no_dtd&proxystylesheet=search_dec_nsw_gov_au_frontend&site=public_ce_dec_nsw_gov_au&access=p&q=",
        debug: false
      };

      // Check if config has been passed to constructor
      if (config) {
        // Merge default config with passed config
        this.config = _jquery.extend(true, {}, this.config, config);
      }

      this.progress = 0;

      (0, _jquery)(window).keyup(function (event) {
        if (_this.checkKeyCommand(event)) {
          _this.build();
          _this.listen();
        }
      });
    }

    /**
     * Exports the HeyGEF class as a module
     * @module
     */

    _createClass(HeyGEF, [{
      key: "build",
      value: function build() {
        var modalDiv = (0, _jquery)("body").append("<div id=\"heyGEF\" class=\"uk-modal\" style=\"z-index: 6000\"><div class=\"uk-modal-dialog\">Listening...</div></div>");
        this.modal = _UI["default"].modal("#heyGEF");
      }
    }, {
      key: "checkKeyCommand",
      value: function checkKeyCommand(event) {
        // If keypress is euqal to currently expected keyCommand
        if (event.which === this.config.keyCommand[this.progress]) {
          if (this.progress === this.config.keyCommand.length - 1) {
            this.progress = 0;
            return true;
          }
          this.progress++;
        } else {
          this.progress = 0;
        }
        return false;
      }
    }, {
      key: "print",
      value: function print(userSaid, commandText, phrases) {
        this.modal.find(".uk-modal-dialog").text(userSaid);
      }
    }, {
      key: "listen",
      value: function listen() {
        if (annyang) {

          if (!this.modal.isActive()) {
            this.modal.show();
          }

          annyang.addCommands(this.config.commands);
          annyang.debug(this.config.debug);
          annyang.addCallback("resultMatch", this.print.bind(this));
          annyang.start();
        }
      }
    }, {
      key: "search",
      value: function search(query) {
        window.location = "" + this.config.searchURI + query;
      }

      /**
       * Static function to instatiate the HeyGEF class as singleton
       *
       * @static
       *
       * @param {String|Element|jQuery} selector - Either a CSS selector, DOM element or matched jQuery object
       * @param {object} config   - class configuration arguments. Refer to class constructor for complete documentation of the config object
       *
       * @returns {ExampleClass} Reference to the same ExampleClass instatiated in memory
       */
    }], [{
      key: "shared",
      value: function shared(selector, config) {
        return this.instance != null ? this.instance : this.instance = new HeyGEF(selector, config);
      }
    }]);

    return HeyGEF;
  })();

  module.exports = HeyGEF;
});