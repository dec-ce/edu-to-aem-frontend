define(["exports", "module", "jquery", "uikit"], function (exports, module, _jquery, _uikit) {
    "use strict";

    function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

    function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

    var _UI = _interopRequireDefault(_uikit);

    /**
     *
     * @since 1.0.0
     *
     * @author Digital Services <communications@det.nsw.edu.au>
     * @copyright © 2015 State Government of NSW 2015
     *
     * @class
     * @requires jQuery
     */

    var ariaToggleMenu =

    /**
     * Script for Google translate feature
     *
     * @constructor
     *
     * @param {String|Element|jQuery} selector - Either a CSS selector, DOM element or matched jQuery object
     * @param {Object} config - class configuration options. Options vary depending on need
     *
     */

    function ariaToggleMenu(selector, config) {
        _classCallCheck(this, ariaToggleMenu);

        // Check if config has been passed to constructor
        if (config) {
            // Merge default config with passed config
            this.config = _jquery.extend(true, {}, this.config, config);
        }

        /* UIKIT's function for dropdown open event 
          This extra bit of coding is needed so that the aria labels get set correctly in the anchor tag */
        (0, _jquery)('[data-uk-dropdown]').on('show.uk.dropdown', function () {
            (0, _jquery)(this).children('a').attr("aria-expanded", "true");
            (0, _jquery)(this).find('.edu-dropdown-menu').removeAttr("aria-hidden");
            //adding the following conditions to fix tab issue in IE11
            //$(this).find('.edu-dropdown-menu').removeAttr('sizset').removeAttr('sizcache');
            //$(this).find('.edu-meganav-container').attr("aria-hidden", "true");
            //$(this).find('.edu-mega-nav-desc').attr("aria-hidden", "true");
            //$(this).find('.edu-meganav-container').focus();
            (0, _jquery)(this).find('.edu-dropdown-menu').attr("tabindex", "0");
            removeAriaAttributes();
        });
        /*  UIKIT's function for dropdown close event*/
        (0, _jquery)('[data-uk-dropdown]').on('hide.uk.dropdown', function () {
            (0, _jquery)(this).children('a').attr("aria-expanded", "false");
            removeAriaAttributes();
        });
        /* Removing unnecessary aria-tags in the li */
        (0, _jquery)('.edu-meganav-megalinks').on('keyup', function (e) {
            var code = e.keyCode ? e.keyCode : e.which;
            if (code == 9) {
                (0, _jquery)(this).parent().prev('.edu-parent-menu').removeClass('uk-open');
                (0, _jquery)(this).parent().prev('.edu-parent-menu').removeAttr("aria-expanded");
                (0, _jquery)(this).parent().prev('.edu-parent-menu').removeAttr("aria-haspopup");
                removeAriaAttributes();
            }
        });
        //identify the last link of the last submenu so that when the user tabs off the megamenu closes
        (0, _jquery)('.edu-mega-menu-title-bar li.edu-parent-menu:last-child').addClass("edu-close-dropdown__lastlist");
        (0, _jquery)('.edu-close-dropdown__lastlist li:last-child').addClass("edu-close-dropdown__lastlist-link");
        (0, _jquery)('.edu-close-dropdown__lastlist-link').on('keydown', function (e) {
            var code = e.keyCode ? e.keyCode : e.which;
            if (code === 9) {
                (0, _jquery)('.edu-close-dropdown__lastlist').removeClass('uk-open');
                (0, _jquery)('.edu-close-dropdown__lastlist').removeAttr("aria-expanded");
                (0, _jquery)('.edu-close-dropdown__lastlist').removeAttr("aria-haspopup");
                UIkit.dropdown('.uk-dropdown').hide();
                (0, _jquery)('uk-dropdown').attr("aria-hidden", "true");
            }
        });
        (0, _jquery)("li.uk-parent.edu-parent-menu").mouseover(function () {
            setTimeout(function () {
                (0, _jquery)(this).each(function () {
                    removeAriaAttributes();
                });
            });
        });
        function removeAriaAttributes() {
            (0, _jquery)('li.uk-parent.edu-parent-menu').removeAttr("aria-expanded");
            (0, _jquery)('li.uk-parent.edu-parent-menu').removeAttr("aria-haspopup");
        }
    };

    module.exports = ariaToggleMenu;
});