define(["exports", "module", "react", "reactAutocomplete", "lodash"], function (exports, module, _react, _reactAutocomplete, _lodash) {
    "use strict";

    function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

    var _React = _interopRequireDefault(_react);

    var _ReactAutocomplete = _interopRequireDefault(_reactAutocomplete);

    var _2 = _interopRequireDefault(_lodash);

    var SearchForm = function SearchForm(props) {
        var providers = props.providers;
        var schools = props.schools;
        var searchinput = props.searchinput;
        var invalidschool = props.invalidschool;
        var onSetSearchInput = props.onSetSearchInput;
        var onSetSelectedSchool = props.onSetSelectedSchool;

        var handleSearch = function handleSearch(event) {
            event.preventDefault();
            var selectedSchool = _2["default"].find(schools, { school: searchinput });
            onSetSelectedSchool(selectedSchool, providers);
        };

        if (!schools) {
            return "";
        } else {
            return _React["default"].createElement(
                "form",
                { id: "frmschool", name: "frmschool", className: "gef-search uk-form gef-search--body", onSubmit: handleSearch },
                _React["default"].createElement(
                    "h2",
                    null,
                    "Enter your school name to view approved providers"
                ),
                _React["default"].createElement(
                    "fieldset",
                    null,
                    _React["default"].createElement(
                        "legend",
                        { className: "show-on-sr" },
                        "Search the approved panel of registered training providers"
                    ),
                    _React["default"].createElement(
                        "label",
                        { htmlFor: "searchrrto" },
                        _React["default"].createElement(
                            "span",
                            { className: "show-on-sr" },
                            "Enter your school name"
                        )
                    ),
                    _React["default"].createElement(_ReactAutocomplete["default"], {
                        inputProps: {
                            id: "searchrrto",
                            className: "ui ui-autocomplete-input",
                            maxLength: 254,
                            type: "text",
                            "aria-haspopup": true
                        },
                        items: _2["default"].toArray(schools),
                        shouldItemRender: function (item, value) {
                            return value.length >= 3 && item.school.toLowerCase().indexOf(value.toLowerCase()) > -1;
                        },
                        getItemValue: function (item) {
                            return item.school;
                        },
                        renderItem: function (item, highlighted) {
                            return _React["default"].createElement(
                                "div",
                                { key: item.code,
                                    style: { backgroundColor: highlighted ? "#eee" : "transparent" } },
                                item.school
                            );
                        },

                        value: searchinput === null ? "" : searchinput,
                        onChange: function (element) {
                            return onSetSearchInput(element.target.value);
                        },
                        onSelect: function (value) {
                            return onSetSearchInput(value);
                        }
                    }),
                    _React["default"].createElement(
                        "button",
                        { type: "button", id: "submit_schoolsearch", "aria-label": "Submit search", onClick: handleSearch },
                        "SEARCH"
                    )
                ),
                invalidschool === null ? "" : _React["default"].createElement(
                    "div",
                    { style: { display: "block" }, id: "no_schoolname" },
                    _React["default"].createElement(
                        "p",
                        null,
                        _React["default"].createElement(
                            "span",
                            { className: "rrtoerror" },
                            invalidschool
                        )
                    )
                )
            );
        }
    };

    module.exports = SearchForm;
});