define(["exports", "module", "reactRedux", "reactPureLifecycle", "apps/health-safety/components/SearchForm/presenter", "apps/health-safety/actions/schools", "apps/health-safety/actions/searchinput", "apps/health-safety/actions/selectedschool"], function (exports, module, _reactRedux, _reactPureLifecycle, _appsHealthSafetyComponentsSearchFormPresenter, _appsHealthSafetyActionsSchools, _appsHealthSafetyActionsSearchinput, _appsHealthSafetyActionsSelectedschool) {
    "use strict";

    function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

    var _lifecycle = _interopRequireDefault(_reactPureLifecycle);

    var _SearchForm = _interopRequireDefault(_appsHealthSafetyComponentsSearchFormPresenter);

    function mapStateToProps(state) {
        var providers = state.providers;
        var schools = state.schools;
        var searchinput = state.searchinput;
        var invalidschool = state.invalidschool;
        return {
            providers: providers,
            schools: schools,
            searchinput: searchinput,
            invalidschool: invalidschool
        };
    }

    function mapDispatchToProps(dispatch) {
        return {
            onGetSchools: function onGetSchools() {
                return dispatch((0, _appsHealthSafetyActionsSchools.getSchools)());
            },
            onSetSearchInput: function onSetSearchInput(searchinput) {
                return dispatch((0, _appsHealthSafetyActionsSearchinput.setSearchInput)(searchinput));
            },
            onSetSelectedSchool: function onSetSelectedSchool(selectedschool, providers) {
                return (0, _appsHealthSafetyActionsSelectedschool.setSelectedSchool)(dispatch, selectedschool, providers);
            }
        };
    }

    var componentWillMount = function componentWillMount(store) {
        store.onGetSchools();
        store.onSetSearchInput("");
    };

    var cwm = (0, _lifecycle["default"])({
        componentWillMount: componentWillMount
    })(_SearchForm["default"]);

    module.exports = (0, _reactRedux.connect)(mapStateToProps, mapDispatchToProps)(cwm);
});