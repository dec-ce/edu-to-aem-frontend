define(["exports", "module", "react"], function (exports, module, _react) {
    "use strict";

    function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

    var _React = _interopRequireDefault(_react);

    var Providers = function Providers(props) {
        var selectedschool = props.selectedschool;
        var selectedproviders = props.selectedproviders;

        if (!selectedschool || !selectedproviders) {
            return "";
        } else {
            return _React["default"].createElement(
                "div",
                { id: "results", style: { display: "block" } },
                _React["default"].createElement(
                    "h2",
                    { id: "school" },
                    selectedschool.school
                ),
                _React["default"].createElement(
                    "p",
                    null,
                    "*Prices shown below are indicative only and exclusive of any travel costs. The maximum 'price per person' cost provided is the ceiling price (inclusive of GST) the provider is able to charge. The price per person cost may vary depending on the amount of people attending a session e.g. 2 people per session may be significantly more expensive than 20 people per session. It is recommended that each school contact at least 3 providers and confirm the 'price per person' cost for the requested training session."
                ),
                _React["default"].createElement(
                    "table",
                    { id: "data_table" },
                    _React["default"].createElement(
                        "thead",
                        null,
                        _React["default"].createElement(
                            "tr",
                            null,
                            _React["default"].createElement(
                                "th",
                                null,
                                "Provider Name"
                            ),
                            _React["default"].createElement(
                                "th",
                                null,
                                "No. of Trainers"
                            ),
                            _React["default"].createElement(
                                "th",
                                { className: "preferredsel" },
                                "Combined CPR & Anaphylaxis*"
                            ),
                            _React["default"].createElement(
                                "th",
                                null,
                                "Provider CPR*"
                            ),
                            _React["default"].createElement(
                                "th",
                                null,
                                "Anaphylaxis*"
                            )
                        )
                    ),
                    _React["default"].createElement(
                        "tbody",
                        null,
                        selectedproviders.map(function (provider, key) {
                            return _React["default"].createElement(
                                "tr",
                                { key: key },
                                _React["default"].createElement(
                                    "td",
                                    null,
                                    _React["default"].createElement(
                                        "strong",
                                        null,
                                        provider.name
                                    ),
                                    _React["default"].createElement("br", null),
                                    provider.contact,
                                    _React["default"].createElement("br", null),
                                    provider.phone,
                                    _React["default"].createElement("br", null),
                                    _React["default"].createElement(
                                        "a",
                                        { href: provider.email },
                                        provider.email
                                    )
                                ),
                                _React["default"].createElement(
                                    "td",
                                    null,
                                    provider.trainers
                                ),
                                _React["default"].createElement(
                                    "td",
                                    { className: "preferredsel" },
                                    provider.combined
                                ),
                                _React["default"].createElement(
                                    "td",
                                    null,
                                    provider.cpr
                                ),
                                _React["default"].createElement(
                                    "td",
                                    null,
                                    provider.anaphylaxis
                                )
                            );
                        })
                    )
                ),
                _React["default"].createElement(
                    "div",
                    { id: "totalprov" },
                    _React["default"].createElement(
                        "p",
                        null,
                        "Total Providers: ",
                        _React["default"].createElement(
                            "span",
                            { id: "num_providers" },
                            selectedproviders.length
                        )
                    )
                )
            );
        }
    };

    module.exports = Providers;
});