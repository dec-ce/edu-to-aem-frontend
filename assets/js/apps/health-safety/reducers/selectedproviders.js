define(["exports", "module", "apps/health-safety/constants/actionTypes"], function (exports, module, _appsHealthSafetyConstantsActionTypes) {
    "use strict";

    function _toConsumableArray(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = Array(arr.length); i < arr.length; i++) arr2[i] = arr[i]; return arr2; } else { return Array.from(arr); } }

    var initialState = null;

    module.exports = function (state, action) {
        if (state === undefined) state = initialState;

        switch (action.type) {
            case _appsHealthSafetyConstantsActionTypes.SELECTEDPROVIDERS_CLEAR:
                return clearSelectedProviders();
            case _appsHealthSafetyConstantsActionTypes.SELECTEDPROVIDERS_ADD:
                return addSelectedProvider(state, action);
            default:
                return state;
        }
    };

    function clearSelectedProviders() {
        return [];
    }

    function addSelectedProvider(state, action) {
        var selectedprovider = action.selectedprovider;

        if (!selectedprovider) {
            return state;
        } else {
            return [].concat(_toConsumableArray(state), [selectedprovider]);
        }
    }
});