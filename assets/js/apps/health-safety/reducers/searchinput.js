define(["exports", "module", "apps/health-safety/constants/actionTypes"], function (exports, module, _appsHealthSafetyConstantsActionTypes) {
    "use strict";

    var initialState = null;

    module.exports = function (state, action) {
        if (state === undefined) state = initialState;

        switch (action.type) {
            case _appsHealthSafetyConstantsActionTypes.SEARCHINPUT_SET:
                return setSearchInput(state, action);
            default:
                return state;
        }
    };

    function setSearchInput(state, action) {
        var searchinput = action.searchinput;

        return searchinput;
    }
});