define(["exports", "module", "apps/health-safety/constants/actionTypes"], function (exports, module, _appsHealthSafetyConstantsActionTypes) {
    "use strict";

    var initialState = [];

    module.exports = function (state, action) {
        if (state === undefined) state = initialState;

        switch (action.type) {
            case _appsHealthSafetyConstantsActionTypes.PROVIDERS_GET_FULFILLED:
                return setProviders(state, action);
            default:
                return state;
        }
    };

    function setProviders(state, action) {
        var providers = action.providers;

        return providers;
    }
});