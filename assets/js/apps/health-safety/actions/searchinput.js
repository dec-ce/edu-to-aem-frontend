define(["exports", "apps/health-safety/constants/actionTypes"], function (exports, _appsHealthSafetyConstantsActionTypes) {
    "use strict";

    Object.defineProperty(exports, "__esModule", {
        value: true
    });
    exports.setSearchInput = setSearchInput;

    function setSearchInput(searchinput) {
        return function (dispatch) {
            dispatch({
                type: _appsHealthSafetyConstantsActionTypes.SEARCHINPUT_SET,
                searchinput: searchinput
            });
        };
    }
});