define(["exports", "apps/health-safety/actions/providers", "apps/health-safety/actions/schools", "apps/health-safety/actions/searchinput", "apps/health-safety/actions/selectedschool", "apps/health-safety/actions/selectedproviders"], function (exports, _appsHealthSafetyActionsProviders, _appsHealthSafetyActionsSchools, _appsHealthSafetyActionsSearchinput, _appsHealthSafetyActionsSelectedschool, _appsHealthSafetyActionsSelectedproviders) {
    "use strict";

    Object.defineProperty(exports, "__esModule", {
        value: true
    });
    exports.getProviders = _appsHealthSafetyActionsProviders.getProviders;
    exports.getSchools = _appsHealthSafetyActionsSchools.getSchools;
    exports.setSearchInput = _appsHealthSafetyActionsSearchinput.setSearchInput;
    exports.setSelectedSchool = _appsHealthSafetyActionsSelectedschool.setSelectedSchool;
    exports.setSelectedProviders = _appsHealthSafetyActionsSelectedproviders.setSelectedProviders;
});