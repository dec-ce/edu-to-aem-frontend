define(["exports", "apps/search/filters/filters"], function (exports, _appsSearchFiltersFilters) {
  "use strict";

  function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

  var _filters = _interopRequireDefault(_appsSearchFiltersFilters);

  // necessary to display HTML as intended, not the code source.
  _filters["default"].filter("sanitize", ['$sce', function ($sce) {
    return function (htmlCode) {
      return $sce.trustAsHtml(htmlCode);
    };
  }]);
});