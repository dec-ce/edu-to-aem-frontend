define(["exports", "apps/search/services/services"], function (exports, _appsSearchServicesServices) {
  "use strict";

  function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

  var _services = _interopRequireDefault(_appsSearchServicesServices);

  _services["default"].factory("utilsService", function () {
    var getQueryString = function getQueryString(field, url) {
      var href = url ? url : window.location.href;
      // splits the URL on ? or & + the field name  + = to the next & or # or end for the value.
      var regex = new RegExp('[?&]' + field + '=([^&#]*)', 'i');
      // use the regex to get he value of the URL key/value pair
      var urlVarValue = regex.exec(href);
      // adding decodeURIComponent to convert URL characters (eg: %20) to readable ones.
      // This is for when the query is from the top search, and the keyword passed in/
      // However, decideURIComponent expects %20 not + for a space, so adding an additional regexp to fix that.
      return urlVarValue ? decodeURIComponent(urlVarValue[1]).replace(/\+/, " ") : "";
    };

    /**
     * Return the site's name based on one of the following:
     * 1) The text found in HTML element with the CSS class "local-header-heading"
     * 2) A data attribute found in the <head> tag
     * 3) If the page sites within "Inside the department"
     * If none of the above result in a site name then null is returned. If null is returned the Search SPA will use "Department of Education" as the default. This defult is set in the config. 
     * 
     * @returns (String) The name of the site or null
     */
    var getSiteName = function getSiteName() {
      var siteName = null;

      if (jQuery(".local-header-heading").length > 0) {
        siteName = jQuery(".local-header-heading").text();
      }

      if (jQuery("head").data("siteName")) {
        siteName = jQuery("head").data("siteName");
      }

      if (window.location.href.indexOf("inside-the-department") > -1) {
        siteName = "Inside the department";
      }

      return siteName;
    };

    return {
      searchTerm: getQueryString("q"),
      siteUrl: jQuery("head").data("siteUrl") ? jQuery("head").data("siteUrl") : getQueryString("sitesearch"),
      siteName: getSiteName()
    };
  });
});