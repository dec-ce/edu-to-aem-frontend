define(["exports", "module", "angular"], function (exports, module, _angular) {
  "use strict";

  function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

  var _angular2 = _interopRequireDefault(_angular);

  module.exports = _angular2["default"].module("aria", ["ngAria"]);
});