define(["exports", "apps/search/directives/directives"], function (exports, _appsSearchDirectivesDirectives) {
  "use strict";

  function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

  var _directives = _interopRequireDefault(_appsSearchDirectivesDirectives);

  _directives["default"].directive("sspaSearchResults", ["configService", "searchService", function (Config, searchService) {
    return {
      restrict: "AE",
      replace: true,
      templateUrl: Config.views.searchResults,
      scope: true
    };
  }]);
});