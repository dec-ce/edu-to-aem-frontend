define(["exports", "apps/search/directives/directives"], function (exports, _appsSearchDirectivesDirectives) {
  "use strict";

  function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

  var _directives = _interopRequireDefault(_appsSearchDirectivesDirectives);

  _directives["default"].directive("sspaSearchForm", ["configService", "searchService", function (Config, searchService) {
    return {
      restrict: "AE",
      replace: true,
      templateUrl: Config.views.searchForm,
      link: function link(scope, element, attributes) {

        scope.keyword = searchService.searchTerm;
        scope.searchTerm = searchService.searchTerm;

        scope.newSearchTerm = function () {
          scope.searchTerm = scope.keyword;
          searchService.pageNumber = 1;
          searchService.getResults(scope.keyword, 1);
        };
      }
    };
  }]);
});