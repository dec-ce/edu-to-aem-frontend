define(["exports", "apps/bushfire-register/constants/actionTypes", "lodash"], function (exports, _appsBushfireRegisterConstantsActionTypes, _lodash) {
    "use strict";

    Object.defineProperty(exports, "__esModule", {
        value: true
    });

    function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

    var _2 = _interopRequireDefault(_lodash);

    var setSelectedSchools = function setSelectedSchools(searchinput, bushfireprone) {
        var selectedschools = _2["default"].reduce(bushfireprone, function (result, value) {
            if (value.name.toLowerCase().indexOf(searchinput.toLowerCase()) > -1) {
                return _2["default"].concat(result, value);
            } else {
                return result;
            }
        }, []);

        if (!selectedschools || selectedschools.length === 0) {
            return function (dispatch) {
                return dispatch({
                    type: _appsBushfireRegisterConstantsActionTypes.SELECTEDSCHOOLS_SET,
                    selectedschools: [{ name: searchinput, bushfireProne: false, hishRisk: false }]
                });
            };
        } else {
            return function (dispatch) {
                return dispatch({
                    type: _appsBushfireRegisterConstantsActionTypes.SELECTEDSCHOOLS_SET,
                    selectedschools: selectedschools
                });
            };
        }
    };
    exports.setSelectedSchools = setSelectedSchools;
});