define(["exports", "axios", "../constants/actionTypes", "lodash"], function (exports, _axios, _constantsActionTypes, _lodash) {
    "use strict";

    Object.defineProperty(exports, "__esModule", {
        value: true
    });
    exports.getBushfireProne = getBushfireProne;

    function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

    var _axios2 = _interopRequireDefault(_axios);

    var _2 = _interopRequireDefault(_lodash);

    function getBushfireProne() {
        return function (dispatch) {
            dispatch(getBushfireProneRequestedAction());

            _axios2["default"].get("https://doe-health-and-safety.firebaseio.com/bushfire.json").then(function (response) {
                var bushfireprone = response.data;
                dispatch(getBushfireProneFulfilledAction(bushfireprone));
                dispatch(getRegions(bushfireprone));
                dispatch(getSchools(bushfireprone));
            })["catch"](function (error) {
                console.log(error);
                dispatch(getBushfireProneRejectedAction());
            });
        };
    }

    function getRegions(bushfireprone) {
        var regionsfull = _2["default"].transform(bushfireprone, function (result, value) {
            result.push(value.nswFireArea);
            return true;
        }, []);

        var sortedregionsfull = _2["default"].sortBy(regionsfull, function (o) {
            return o;
        });
        var regions = _2["default"].sortedUniq(sortedregionsfull);

        return {
            type: _constantsActionTypes.REGIONS_SET,
            regions: regions
        };
    }

    function getSchools(bushfireprone) {
        var unsortedschools = _2["default"].transform(bushfireprone, function (result, value) {
            result.push(value.name);
            return true;
        }, []);

        var schools = _2["default"].sortBy(unsortedschools, function (o) {
            return o;
        });
        return {
            type: _constantsActionTypes.SCHOOLS_SET,
            schools: schools
        };
    }

    function getBushfireProneRequestedAction() {
        return {
            type: _constantsActionTypes.BUSHFIREPRONE_GET_REQUESTED
        };
    }

    function getBushfireProneFulfilledAction(bushfireprone) {
        return {
            type: _constantsActionTypes.BUSHFIREPRONE_GET_FULFILLED,
            bushfireprone: bushfireprone
        };
    }

    function getBushfireProneRejectedAction() {
        return {
            type: _constantsActionTypes.BUSHFIREPRONE_GET_REJECTED
        };
    }
});