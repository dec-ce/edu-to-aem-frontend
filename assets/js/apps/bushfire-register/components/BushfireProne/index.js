define(["exports", "module", "reactRedux", "reactPureLifecycle", "apps/bushfire-register/components/BushfireProne/presenter"], function (exports, module, _reactRedux, _reactPureLifecycle, _appsBushfireRegisterComponentsBushfirePronePresenter) {
    "use strict";

    function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

    var _lifecycle = _interopRequireDefault(_reactPureLifecycle);

    var _BushfireProne = _interopRequireDefault(_appsBushfireRegisterComponentsBushfirePronePresenter);

    function mapStateToProps(state) {
        var selectedschools = state.selectedschools;
        return {
            selectedschools: selectedschools
        };
    }

    var componentWillMount = function componentWillMount(store) {};

    var cwm = (0, _lifecycle["default"])({
        componentWillMount: componentWillMount
    })(_BushfireProne["default"]);

    module.exports = (0, _reactRedux.connect)(mapStateToProps)(cwm);
});