define(["exports", "module", "react"], function (exports, module, _react) {
    "use strict";

    function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

    var _React = _interopRequireDefault(_react);

    var BushfireProne = function BushfireProne(props) {
        var selectedschools = props.selectedschools;

        if (selectedschools) {
            return _React["default"].createElement(
                "div",
                { id: "results", style: { display: "block" } },
                _React["default"].createElement(
                    "div",
                    { className: "gef-call-out-box", id: "bf_results_text" },
                    selectedschools.map(function (school, key) {
                        if (school.highRisk) {
                            return _React["default"].createElement(
                                "p",
                                { key: key },
                                school.name,
                                " in the NSW fire area of ",
                                school.nswFireArea,
                                " is bushfire prone. It is at ",
                                _React["default"].createElement(
                                    "strong",
                                    null,
                                    "increased risk"
                                ),
                                " in the event of bushfire and is on the department\"s bushfire register."
                            );
                        } else if (school.bushfireProne) {
                            return _React["default"].createElement(
                                "p",
                                { key: key },
                                school.name,
                                " in the NSW fire area of ",
                                school.nswFireArea,
                                " is ",
                                _React["default"].createElement(
                                    "strong",
                                    null,
                                    "bushfire prone."
                                )
                            );
                        } else {
                            // Actually this should never happen
                            return _React["default"].createElement(
                                "p",
                                { key: key },
                                school.name,
                                " is ",
                                _React["default"].createElement(
                                    "strong",
                                    null,
                                    "not"
                                ),
                                " in an increased risk or bushfire-prone area."
                            );
                        }
                    })
                )
            );
        } else {
            return "";
        }
    };

    module.exports = BushfireProne;
});