define(["exports", "module", "../constants/actionTypes"], function (exports, module, _constantsActionTypes) {
    "use strict";

    var initialState = null;

    module.exports = function (state, action) {
        if (state === undefined) state = initialState;

        switch (action.type) {
            case _constantsActionTypes.SEARCHINPUT_SET:
                return setSearchInput(state, action);
            default:
                return state;
        }
    };

    function setSearchInput(state, action) {
        var searchinput = action.searchinput;

        return searchinput;
    }
});