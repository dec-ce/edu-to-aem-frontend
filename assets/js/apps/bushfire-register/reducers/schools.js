define(["exports", "module", "../constants/actionTypes"], function (exports, module, _constantsActionTypes) {
    "use strict";

    var initialState = null;

    module.exports = function (state, action) {
        if (state === undefined) state = initialState;

        switch (action.type) {
            case _constantsActionTypes.SCHOOLS_SET:
                return setSchools(state, action);
            default:
                return state;
        }
    };

    function setSchools(state, action) {
        var schools = action.schools;

        return schools;
    }
});