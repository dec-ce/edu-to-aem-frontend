define(["exports", "module", "../constants/actionTypes"], function (exports, module, _constantsActionTypes) {
    "use strict";

    var initialState = null;

    module.exports = function (state, action) {
        if (state === undefined) state = initialState;

        switch (action.type) {
            case _constantsActionTypes.SELECTEDSCHOOLS_SET:
                return setSelectedSchools(state, action);
            default:
                return state;
        }
    };

    function setSelectedSchools(state, action) {
        var selectedschools = action.selectedschools;

        return selectedschools;
    }
});