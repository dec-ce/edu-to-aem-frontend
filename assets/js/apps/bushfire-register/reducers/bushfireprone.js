define(["exports", "module", "../constants/actionTypes"], function (exports, module, _constantsActionTypes) {
    "use strict";

    var initialState = null;

    module.exports = function (state, action) {
        if (state === undefined) state = initialState;

        switch (action.type) {
            case _constantsActionTypes.BUSHFIREPRONE_GET_FULFILLED:
                return setBushfireProne(state, action);
            default:
                return state;
        }
    };

    function setBushfireProne(state, action) {
        var bushfireprone = action.bushfireprone;

        return bushfireprone;
    }
});