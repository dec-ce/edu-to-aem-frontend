define(["exports", "module", "react", "react-dom", "reactRedux", "apps/bushfire-register/store/configureStore", "apps/bushfire-register/App"], function (exports, module, _react, _reactDom, _reactRedux, _appsBushfireRegisterStoreConfigureStore, _appsBushfireRegisterApp) {
  "use strict";

  var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();

  function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

  var _React = _interopRequireDefault(_react);

  var _ReactDOM = _interopRequireDefault(_reactDom);

  var _configureStore = _interopRequireDefault(_appsBushfireRegisterStoreConfigureStore);

  var _App = _interopRequireDefault(_appsBushfireRegisterApp);

  /**
   * Bushfire Register single page app
   *
   * @since 1.2.0
   *
   * @author Digital Services <communications@det.nsw.edu.au>
   * @copyright © 2018 State Government of NSW
   *
   * @class
   */

  var BushfireRegister = (function () {
    function BushfireRegister(selector, config) {
      _classCallCheck(this, BushfireRegister);

      // Check if only one argument has been passed
      if (arguments.length === 1) {
        // If argument[0] is a config object then set the config arg and nullify the selector arg
        if (selector instanceof Object && !(selector instanceof $)) {
          config = selector;
          selector = null;
        }
      }

      this.container = selector;

      // Default class config options
      this.config = {};

      // Check if config has been passed to constructor
      if (config) {
        // Merge default config with passed config
        this.config = $.extend(true, {}, this.config, config);
      }

      var store = (0, _configureStore["default"])();

      _ReactDOM["default"].render(_React["default"].createElement(
        _reactRedux.Provider,
        { store: store },
        _React["default"].createElement(_App["default"], null)
      ), this.container);
    }

    /**
     * Exports the HeathSafety class as a module
     * @module
     */

    _createClass(BushfireRegister, null, [{
      key: "shared",
      value: function shared(selector, config) {
        return this.instance != null ? this.instance : this.instance = new BushfireRegister(selector, config);
      }
    }]);

    return BushfireRegister;
  })();

  module.exports = BushfireRegister;
});