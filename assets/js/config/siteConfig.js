define(["exports", "module"], function (exports, module) {
  "use strict";

  var noticeRibbonCookie = function noticeRibbonCookie() {
    var cookies = document.cookie;
    // see if cookie exists
    if (document.cookie.match(/^(.*;)?\s*noticeRibbonFirstTime\s*=\s*[^;]+(.*)?$/)) {
      return false;
    } else {
      // set the notice ribbon cookie
      var noticeRibbonMaxAge = 5 * 60; // 5 minutes for testing
      // let noticeRibbonMaxAge = 60*60*24*31*6 // 6 months for prod
      document.cookie = "noticeRibbonFirstTime=true;max-age=" + noticeRibbonMaxAge + ";";
      return true;
    }
  };

  var configuration = {
    name: "GEF Education Theme",
    components: {

      "InputSharePOC": {
        "selector": "[data-gef-input-transferer]",
        "script": "utilities/inputValueTransferer",
        "singleton": false
      },

      "backToTopShow": {
        "selector": "body",
        "script": "utilities/scrollToggle",
        "config": {
          "checkpoint": 1500,
          "subject": ".gef-btt",
          "passed_attr": "data-show",
          "scroll_window": true
        }
      },

      "backToTopStick": {
        "selector": "html",
        "script": "utilities/scrollToggle",
        "config": {
          "checkpoint": ".gef-section-footer",
          "subject": ".gef-btt",
          "passed_attr": "data-stick",
          "scroll_window": true
        }
      },

      "backToTopStickHome": {
        "selector": ".edu-home",
        "script": "utilities/scrollToggle",
        "config": {
          "checkpoint": ".gef-global-footer",
          "subject": ".gef-btt",
          "passed_attr": "data-stick",
          "scroll_window": true
        }
      },

      "backToTopStickPolicy": {
        "selector": "html",
        "script": "utilities/scrollToggle",
        "config": {
          "checkpoint": ".gef-global-footer",
          "subject": ".policy .gef-btt",
          "passed_attr": "data-stick",
          "scroll_window": true
        }
      },

      "AnchorHighlight": {
        "selector": ".gef-a-z-anchors",
        "script": "components/anchorHighlight",
        "singleton": false
      },

      "anchorScroll": {
        "selector": "body",
        "script": "utilities/scrollToggle",
        "config": {
          "checkpoint": "[data-gef-a-z-anchors-scroll]",
          "subject": ".ac",
          "passed_attr": "data-scroll-passed",
          "scroll_window": true,
          "scroller_position": "top"
        }
      },

      "edconnectAccessibility": {
        "selector": ".gef-drawer",
        "script": "utilities/ariaToggler",
        "config": {
          "click_outside": true,
          "tabindex_toggle": false
        }
      },

      "ermIconLabelToggler": {
        "selector": ".erm-icon-label-toggle",
        "script": "utilities/ariaToggler",
        "config": {
          "click_outside": true,
          "selectors": {
            "btnclose": "[data-erm-button-close-toggle]"
          }
        }
      },

      "edconnectToggle": {
        "selector": ".gef-drawer a",
        "script": "utilities/classManipulator",
        "config": {
          "event_type": "click",
          "subject": ".gef-drawer",
          "toggle": true,
          "click_outside": true,
          "click_outside_ignores": ".gef-drawer p, .gef-drawer h2, .gef-drawer small"
        }
      },

      "popupOverlay": {
        "selector": ".policy-enquires-popup-heading",
        "script": "components/popupOverlay",
        "config": {
          "event_type": "click",
          "class_name": "policy-enquires-popup",
          "subject": ".policy-landing-header-right",
          "toggle": false,
          "click_outside": false,
          "insertBeforeElement": "policy-search",
          "backgroundClass": "policy-enquires-popup-background"
        }
      },

      "policyFormRadioKeyboard": {
        "selector": ".policy-radios",
        "script": "utilities/submitOnEvent",
        "config": {
          "selector": ".policy-radios",
          "keys": { "keyup": "return" },
          "triggers": "keyup",
          "submitForm": "#policy-form",
          test: false
        }
      },

      "policyFormRadio": {
        "selector": ".policy-radios",
        "script": "utilities/submitOnEvent",
        "config": {
          "selector": ".policy-radios",
          "triggers": "mouseup touchend",
          "submitForm": "#policy-form",
          test: false
        }
      },

      "policyFormDropdown": {
        "selector": ".policy-dropdown-container",
        "script": "utilities/submitOnEvent",
        "config": {
          "selector": ".policy-dropdown-container select",
          "triggers": "change",
          "submitForm": "#policy-form",
          test: false
        }
      },

      "edconnectScrollStop": {
        "selector": "body",
        "script": "utilities/scrollToggle",
        "config": {
          "checkpoint": ".gef-section-footer",
          "subject": ".gef-drawer",
          "passed_attr": "data-scroll-passed",
          "scroller_position": "bottom",
          "scroll_window": true
        }
      },

      "edconnectScrollStopGlobalFooter": {
        "selector": "body.private-primary, body.public-primary, body.secondary-hub",
        "script": "utilities/scrollToggle",
        "config": {
          "checkpoint": ".gef-global-footer",
          "subject": ".gef-drawer",
          "passed_attr": "data-scroll-passed",
          "scroller_position": "bottom",
          "scroll_window": true
        }
      },

      "backToTopScrollStopGlobalFooter": {
        "selector": "body.private-primary, body.public-primary, body.secondary-hub",
        "script": "utilities/scrollToggle",
        "config": {
          "checkpoint": ".gef-global-footer",
          "subject": ".gef-btt",
          "passed_attr": "data-stick",
          "scroll_window": true
        }
      },

      "mobileShowHides": {
        "selector": ".edu-mobile-show-hide",
        "script": "utilities/ariaToggler",
        "config": {}
      },

      "mobileShowHidesForceLinkOnQuery": {
        "selector": ".edu-mobile-show-hide__header a",
        "script": "utilities/forceLinkOnQuery",
        "config": {
          "media_query": "(min-width: 768px)"
        }
      },
      "mobileNavAccessibility": {
        "selector": ".edu-local-mobile-nav",
        "script": "utilities/mobileNavAccessibility",
        "config": {
          "hello": "hello"
        }
      },
      "ariaToggleMenu": {
        "selector": ".edu-mega-menu",
        "script": "utilities/ariaToggleMenu",
        "config": {
          "subject": "li.uk-parent.edu-parent-menu"
        }
      },

      "tabCalendar": {
        "selector": "[calendar-gef-tabs]",
        "script": "components/tabs",
        "config": {
          "breakpoint": "screen"
        }
      },

      "removeAttributesOnMobileShowHidesOnDesktop": {
        "selector": ".edu-mobile-show-hide__content, .edu-mobile-show-hide__header",
        "script": "utilities/changeHtmlAttributesOnQuery",
        "config": {
          "media_query": "(min-width: 768px)",
          "attributes_to_remove": "role aria-hidden aria-controls aria-expanded",
          "attribute_changes": { "tabindex": "-1" }
        }
      }

    },

    requireConfig: {}

  };

  // Update RequireJS config options
  require.config(configuration.requireConfig);

  module.exports = configuration;
});