define(["exports", "module", "core/observable"], function (exports, module, _coreObservable) {
  "use strict";

  var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();

  var _get = function get(_x, _x2, _x3) { var _again = true; _function: while (_again) { var object = _x, property = _x2, receiver = _x3; _again = false; if (object === null) object = Function.prototype; var desc = Object.getOwnPropertyDescriptor(object, property); if (desc === undefined) { var parent = Object.getPrototypeOf(object); if (parent === null) { return undefined; } else { _x = parent; _x2 = property; _x3 = receiver; _again = true; desc = parent = undefined; continue _function; } } else if ("value" in desc) { return desc.value; } else { var getter = desc.get; if (getter === undefined) { return undefined; } return getter.call(receiver); } } };

  function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

  function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

  var _Observable2 = _interopRequireDefault(_coreObservable);

  /**
   * NotificationCentre is a singleton that can be used to broadcasting information across an app
   *
   * @version 1.0
   * @author NSW Department of Education and Communities
   *
   * @class
   * @extends Observable
   */

  var NotificationCentre = (function (_Observable) {
    _inherits(NotificationCentre, _Observable);

    function NotificationCentre() {
      _classCallCheck(this, NotificationCentre);

      _get(Object.getPrototypeOf(NotificationCentre.prototype), "constructor", this).apply(this, arguments);
    }

    _createClass(NotificationCentre, null, [{
      key: "shared",

      /**
       * Returns a pointer to a shared instantiated NotificationCentre object i.e. a singleton
       *
       * @returns {NotificationCentre} NotificationCentre object
       */
      value: function shared() {
        this.instance != null ? this.instance : this.instance = new NotificationCentre();
        return this.instance;
      }
    }]);

    return NotificationCentre;
  })(_Observable2["default"]);

  module.exports = NotificationCentre;
});