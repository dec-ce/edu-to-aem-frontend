define(["exports", "module"], function (exports, module) {
  "use strict";

  /**
   * Abstract class that provides publisher/subscriber (a.k.a observer) methods
   *
   * @version 1.0
   * @author NSW Department of Education and Communities
   *
   * @class
   */

  var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();

  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

  var Observable = (function () {
    function Observable() {
      _classCallCheck(this, Observable);

      this.channels = {};
    }

    /**
     * Subscribe to a publisher's event providing a callback function to execute
     *
     * @param {String} name - The name of the event to listen for
     * @param {Function} callback - Callback function to excute for the related event
     *
     * @return {Object} A reference to the publisher object
     */

    _createClass(Observable, [{
      key: "subscribe",
      value: function subscribe(name, callback) {

        if (this.channels[name] == null) {
          this.channels[name] = [];
        }

        // Add the callback to the event channel, mapping the instance object and callback to properties of an object literal
        this.channels[name].push({
          // 'this' is a reference to the instance object. Don't forget this JS is a mixin ;)
          context: this,
          callback: callback
        });

        return this;
      }

      /**
       * Return an array of subscribers to a particular channel (a.k.a. event)
       *
       * @param {String} name - The name of the event
       *
       * @returns {Array} Array of subscribers (callback functions)
       */
    }, {
      key: "subscribers",
      value: function subscribers(name) {
        // Return the registered subscribers for the "named" channel
        return this.channels[name] || [];
      }

      /**
       * Unsubscribe from a publisher's event
       *
       * @param name [String] The name of the event
       * @param callback [Function|Boolean] Callback function to remove OR 'true' (boolean value) to remove ALL subscribers from the channel
       *
       * @returns {Array} Array of removed callbacks/subscribers
       */
    }, {
      key: "unsubscribe",
      value: function unsubscribe(name, callback) {
        var removeAll = false;

        if (typeof callback === "boolean" && callback === true) {
          removeAll = true;
        }

        // Reference to array of subscribers for a channel
        var subscribers = this.channels[name];
        var results = [];

        // Loop through array of subscribers
        for (var i = 0, _length = subscribers.length; i < _length; i++) {
          var subscriber = subscribers[i];

          // Compare the callback function registered in the array of subscribers against the passed callback. If it's a mactch drop it from the array of subscribers. Alternatively if the 'all' boolean is 'true' then drop the callback regardless of match
          if (subscriber.callback === callback || removeAll === true) {
            // Push the removed callback onto the results array
            results.push(this.channels[name].splice(i, 1));
          }
        }

        return results;
      }

      /**
       * Trigger an event and excute registered callback functions
       *
       * @param {String} name - The name of the event
       * @param {...*} data - Zero or more variables of any type
       */
    }, {
      key: "publish",
      value: function publish() {
        // The first argument is the name of the channel
        var name = arguments[0];
        // All other arguments (variable number of passed arguments). If there are no arguments passed except for the 'name' argument then simply set the 'data' variable to an empty array
        var data = arguments.length >= 2 ? Array.prototype.slice.call(arguments, 1) : [];
        var results = [];

        if (this.channels[name]) {
          for (var i = 0, _length2 = this.channels[name].length; i < _length2; i++) {
            var subscriber = this.channels[name][i];
            // Fire the callback passing the subscriber's context as the 'this' context and the variable data
            results.push(subscriber.callback.apply(subscriber.context, data));
          }
        }

        return results;
      }
    }]);

    return Observable;
  })();

  module.exports = Observable;
});