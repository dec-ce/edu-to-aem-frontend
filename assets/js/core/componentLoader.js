define(["exports", "module", "core/notificationCentre", "jquery"], function (exports, module, _coreNotificationCentre, _jquery) {
  "use strict";
  var _bind = Function.prototype.bind;

  var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();

  function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

  function _toConsumableArray(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = Array(arr.length); i < arr.length; i++) arr2[i] = arr[i]; return arr2; } else { return Array.from(arr); } }

  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

  var _NotificationCentre = _interopRequireDefault(_coreNotificationCentre);

  var ComponentLoader = (function () {

    /**
     * Create a new ComponentLoader
     *
     * @param {PlainObject} themeConfig -
     * @param {String} themeConfig.name -
     * @param {PlainObject} themeConfig.components -
     *
     * @returns {Loader}
     */

    function ComponentLoader(themeConfig) {
      var _this = this;

      _classCallCheck(this, ComponentLoader);

      // Get a reference to the NotificationCentre singleton
      this.notificationCentre = _NotificationCentre["default"].shared();

      // Subscribe to diagnostic events broadcast by other classes
      this.notificationCentre.subscribe("domUpdate", function (containerElement) {
        return _this.update(containerElement);
      });

      // Check to make sure we have been supplied with a config
      if (themeConfig) {
        // If the themeConfig is a string representing a path to a config JSON file fetch the file
        if (typeof themeConfig === "string") {
          this.fetchConfig(themeConfig);
        } else {
          // Else load the components
          this.config = themeConfig;
          this.loadComponents(themeConfig);
        }
      }
    }

    /**
     * Fetches a config file via an AJAX request and upon sucessful load will initiate the loading of components
     *
     * @param {String} configPath - Either a relative (to loader.js) or abosolute URL to the theme config
     **/

    _createClass(ComponentLoader, [{
      key: "fetchConfig",
      value: function fetchConfig(configPath) {
        var _self = this;

        _jquery.ajax({
          url: configPath,
          success: function success(data, status, xhr) {
            _self.loadComponents(data);
          },
          error: function error(xhr, status, _error) {
            console.log("Failed to load theme config.", _error);
          }
        });
      }

      /**
       * Load any components found in a specified container element. Relies on theme config already having been loaded
       * 
       * @param {String|Element|jQuery} containerElement - Either a CSS selector, DOM element or matched jQuery object
       */
    }, {
      key: "update",
      value: function update(containerElement) {
        if (!this.config) {
          throw new Error("Theme config missing. Please load a config by supplying a config path/object to the constructor or the loadComponents() function.");
        }

        this.loadComponents(this.config, containerElement);
      }

      /**
       * Load 1 or more componets specified
       *
       * @param {PlainObject|String} themeConfig - Theme config object or a path to a config JSON file
       * @param {String} themeConfig.name - The name of the theme
       * @param {PlainObject} themeConfig.components - A POJO with 1 or more components defined with user specified key and the components config as the value
       */
    }, {
      key: "loadComponents",
      value: function loadComponents(themeConfig, containerElement) {

        // If the themeConfig is a string representing a path to a config JSON file fetch the file
        if (typeof themeConfig === "string") {
          this.fetchConfig(themeConfig);
          return;
        }

        this.config = themeConfig;

        for (var componentKey in themeConfig.components) {
          this.loadComponent(themeConfig.components[componentKey], containerElement);
        }
      }

      /**
       * Load a single component
       *
       * @param {PlainObject} componentConfig -
       * @param {String} componentConfig.selector -
       * @param {String} componentConfig.script -
       * @param {PlainObject} componentConfig.config -
       * @param {Function} componentConfig.test -
       * @param {Boolean} componentConfig.singleton -
       */
    }, {
      key: "loadComponent",
      value: function loadComponent(componentConfig, containerElement) {
        if (!componentConfig) {
          return new Error("Could not load component. Component config is required");
        }

        // Deafult component config model
        var componentDefaults = {
          selector: null,
          script: null,
          config: null,
          test: null,
          singleton: false
        };

        // Merge componentDefinition into the default component
        componentConfig = _jquery.extend(componentDefaults, componentConfig);

        // Don't attempt to load the component definition if it doens"t have a
        // "selector" property set
        if (componentDefaults.selector === undefined) {
          return new Error("Could not load component. The component's selector property was not set");
        }

        // Don't attempt to load the component definition if it doens"t have a
        // "script" property set
        if (!componentConfig.script) {
          return new Error("Could not load component. The component's script property was not set");
        }

        // If the component has a test callback and the callback returns false
        // then don't load the component
        if (componentConfig.test !== null && componentConfig.test() === false) {
          return false;
        }

        var $presentComponents = undefined;

        // If containerElement is a build a new CSS selector string that scopes the componentConfig.selector to the containerElement
        if (typeof containerElement === "string") {
          $presentComponents = (0, _jquery)(containerElement + " " + componentConfig.selector);

          // If containerElement is a HTML element or matched jQuery object then use the "context" param of $ to scope the componentConfig.selector to the containerElement
        } else if (containerElement instanceof Element || containerElement instanceof _jquery) {
            $presentComponents = (0, _jquery)(componentConfig.selector, containerElement);

            // Otherwise perform a jQuery match that is globaly scoped i.e. the whole page
          } else {
              $presentComponents = (0, _jquery)(componentConfig.selector);
            }

        // Find all the compontents with matching our compontents selector string
        // let $presentComponents = $(componentConfig.selector)

        // If the component is not present in the DOM then don't load the component
        if ($presentComponents.length < 0) {
          return false;
        }

        var _self = this;

        // Loop through each instance of the compontent that jQuery found
        _jquery.each($presentComponents, function (index, componentInstance) {

          var promisedCompontent = _self.getComponent(componentConfig.script);

          promisedCompontent.done(function (ComponentClass) {
            _self.initComponent(ComponentClass, componentInstance, componentConfig);
          });

          promisedCompontent.fail(function (error) {
            console.log(error, "Unable to load component.", componentConfig);
          });
        });
      }

      /**
       * Instantiate a new GEF module
       *
       * @param {Class} ComponentClass - a URL/path to a AMD compliant JS module
       * @param {jQuery} componentInstance - The element to pass to the ComponentClass as the first argument
       * @param {componentConfig} - The component config definition to pass to the ComponentClass as the second argument
       *
       * @returns {Object|Error} Returns an instance of the passed Class or an Error object
       */
    }, {
      key: "initComponent",
      value: function initComponent(ComponentClass, componentInstance, componentConfig) {

        try {

          // Ideally typeof ComponentClass should be of type 'object' but because AMD modules are wrapped in a define function it returns a type of 'function'
          if (typeof ComponentClass === "function") {
            var component = undefined;

            if (componentConfig.singleton) {
              // If the component should be instantiated as a singleton

              if (componentConfig.hasOwnProperty("args")) {
                component = ComponentClass.shared.apply(componentConfig.args);
              } else {
                component = ComponentClass.shared(componentInstance, componentConfig.config);
              }
            } else {
              // else instatiate as a regular class

              if (componentConfig.hasOwnProperty("args")) {
                component = new (_bind.apply(ComponentClass, [null].concat(_toConsumableArray(componentConfig.args))))();
              } else {
                component = new ComponentClass(componentInstance, componentConfig.config);
              }
            }

            return component;
          } else {
            return new Error("Error loading component: " + componentConfig.script);
          }
        } catch (error) {
          return new Error("Error loading component: " + componentConfig.script);
        }
      }

      /**
       * Get a JS AMD compliant module using RequireJS
       *
       * @param {String} scriptURL - a URL/path to a AMD compliant JS module
       *
       * @returns {Promise} Returns a jQuery Promise which resolved to a RequireJS's sucess callback response
       */
    }, {
      key: "getComponent",
      value: function getComponent(scriptURL) {
        var $deferred = _jquery.Deferred();

        // Use RequireJS require() function to
        require(
        // Use RequireJS to load the compontent JS file
        [scriptURL],
        // On success
        function (ComponentClass) {
          $deferred.resolve(ComponentClass);
        },
        // On error
        function (error) {
          $deferred.reject(error);
        });

        // Return a jQuery Promise
        return $deferred.promise();
      }
    }]);

    return ComponentLoader;
  })();

  module.exports = ComponentLoader;
});