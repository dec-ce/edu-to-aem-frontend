define(["exports", "module", "uikit!datepicker", "jquery"], function (exports, module, _uikitDatepicker, _jquery) {
  "use strict";

  var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();

  function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

  var _UI = _interopRequireDefault(_uikitDatepicker);

  /**
  * ui-kit-datepicker - UI Kit datepicker with accessibility enhancements.
  *
  * @class
  * @requires jQuery
  */

  var Datepicker = (function () {

    /* @param {string} selector - CSS selector or jQuery object of HTML element to use */

    function Datepicker(selector, config) {
      _classCallCheck(this, Datepicker);

      this.config = {
        $selector: (0, _jquery)(selector)
      };

      this.$selector = this.config.$selector;
      this.initDatepicker();
    }

    _createClass(Datepicker, [{
      key: "initDatepicker",
      value: function initDatepicker() {
        // Define input element that the datepicker attaches to.
        var inputElement = this.$selector[0];
        // moveDatepicker function fires when input element is interacted with.
        this.$selector.on("keydown mousedown focus touchstart", this.moveDatepicker.bind(this));
      }
    }, {
      key: "moveDatepicker",
      value: function moveDatepicker() {
        // Define element of the datepicker
        var targetElement = document.querySelectorAll('.uk-datepicker');
        // Moves datepicker element near the input field.
        this.$selector.after(targetElement[0]);
        // Retrieve the left position of the input field.
        var leftPosition = this.$selector.position().left;
        //Finds the height of the input field and adds this to the top position.
        //This is so the datepicker sits at the bottom of input field.
        var inputHeight = this.$selector[0].offsetHeight;
        var topPosition = this.$selector.position().top + inputHeight;
        // Positions datepicker component
        setTimeout(function () {
          (0, _jquery)('.uk-datepicker').css('top', topPosition);
          (0, _jquery)('.uk-datepicker').css('left', leftPosition);
        }, 5);
        // Positioning for mobile
        if (window.innerWidth < 450) {
          //mobile positioning
          var a = window.innerWidth;
          var b = (0, _jquery)('.uk-datepicker').outerWidth();
          setTimeout(function () {
            (0, _jquery)('.uk-datepicker').css('left', (a - b) / 2);
          }, 5);
          //Clear placeholder
          this.$selector.attr('placeholder', '');
        }
        // Hide datepicker if click outside the datepicker (this is needed for some tablets)
        (0, _jquery)('body').bind('touchstart', function (e) {
          if ((0, _jquery)(e.target).closest('.uk-datepicker').length == 0 && (0, _jquery)(e.target).closest('.gef-date-picker').length == 0) {
            (0, _jquery)('.uk-datepicker').hide();
          }
        });
        //Call ARIAlabels function
        this.ariaLabels();
      }
    }, {
      key: "ariaLabels",
      value: function ariaLabels() {
        //Add labels to the previous next button
        (0, _jquery)('.uk-datepicker-previous').html('<span class="show-on-sr">Previous month</span>');
        (0, _jquery)('.uk-datepicker-next').html('<span class="show-on-sr">Next month</span>');
        //This loop iterates over the anchors within every td and adds an aria label with the date
        (0, _jquery)(".uk-datepicker-table td a").each(function (index) {
          var day = (0, _jquery)(this).html(),
              months = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"],
              defaultDate = (0, _jquery)(this).attr('data-date'),
              year = defaultDate.substring(0, 4),
              month = defaultDate.substring(5, 7);
          month = parseInt(month) - 1;
          month = months[month];
          var ariadate = day + " " + month + " " + year;
          (0, _jquery)(this).attr('aria-label', ariadate);
        });
        //Attach ARIA labels to days of the week.
        var counter = 0;
        (0, _jquery)(".uk-datepicker-table th").each(function (index) {
          var daysOfWeek = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday'];
          (0, _jquery)(this).attr('aria-label', daysOfWeek[counter]);
          counter++;
        });
      }
    }]);

    return Datepicker;
  })();

  module.exports = Datepicker;
});