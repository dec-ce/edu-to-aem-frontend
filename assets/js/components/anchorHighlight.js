define(["exports", "module", "jquery", "utilities/anchorFilter", "utilities/scrollToggle"], function (exports, module, _jquery, _utilitiesAnchorFilter, _utilitiesScrollToggle) {
  "use strict";

  function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

  var _AnchorFilter = _interopRequireDefault(_utilitiesAnchorFilter);

  var _ScrollToggle = _interopRequireDefault(_utilitiesScrollToggle);

  var AnchorHighlight = function AnchorHighlight(selector) {
    _classCallCheck(this, AnchorHighlight);

    this.scroll_config = {
      "subject": "[data-character-name] a",
      "scroller_position": "percent",
      "scroller_percent": 5,
      "linked": true,
      "scroll_window": true,
      "toggle_class": "active"
    };

    // Instantiate the AnchorFilter
    this.anchor = new _AnchorFilter["default"](selector);

    //Remove unneeded attributes
    (0, _jquery)('span').parent().removeAttr('data-character-name');

    // Instantiate the ScrollToggle
    this.scroll = new _ScrollToggle["default"](selector, this.scroll_config);

    //IE fix
    if (navigator.appName == 'Microsoft Internet Explorer' || !!(navigator.userAgent.match(/Trident/) || navigator.userAgent.match(/rv 11/)) || typeof _jquery.browser !== "undefined" && _jquery.browser.msie == 1) {
      (0, _jquery)("li a[href*='#Q']").each(function () {
        (0, _jquery)(this).attr('href', '#Qu');
      });
      (0, _jquery)("li span[id*='Q']").each(function () {
        (0, _jquery)(this).attr('id', 'Qu');
      });
    }

    // Highlight bottom letters that wont be triggered by scrollToggle
    (0, _jquery)('.gef-a-z-anchors li a').click(function () {
      var _this = (0, _jquery)(this);
      window.onscroll = function (ev) {
        if (_this != null) {
          (0, _jquery)('.gef-a-z-anchors li a').removeClass('active');
          _this.addClass('active');
          _this = null;
        } else {
          _this = null;
        }
      };
      if (window.innerHeight + window.pageYOffset >= document.body.offsetHeight) {
        if (_this != null) {
          (0, _jquery)('.gef-a-z-anchors li a').removeClass('active');
          _this.addClass('active');
        } else {
          _this = null;
        }
      }
    });
  };

  module.exports = AnchorHighlight;
});