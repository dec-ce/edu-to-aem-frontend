define(["exports", "module", "jquery"], function (exports, module, _jquery) {
  "use strict";

  var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();

  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

  /**
  * dialogue.js - A popup modal which appears after a user interaction
  *
  * @since 0.2.02
  *
  * @author Digital Services <communications@det.nsw.edu.au>
  * @copyright © 2016 State Government of NSW 2016
  *
  * @class
  * @requires jQuery
  */

  var Dialogue = (function () {

    /**
    * Creates a new Dialogue Scenario
    *
    * @constructor
    *
    * @param {String}   selector - the jQuery selector in question
    */

    function Dialogue(selector, config) {
      _classCallCheck(this, Dialogue);

      this.config = {
        $trigger: (0, _jquery)(selector)
      };

      // Check if config has been passed to constructor
      if (config) {
        // Merge default config with passed config
        this.config = _jquery.extend(true, {}, this.config, config);
      }

      // Set the template if it hasn't been passed via the config
      if (this.config.template.html === undefined) {}

      this.initialise();
    }

    /**
    * Something here
    *
    * @initialise
    *
    */

    _createClass(Dialogue, [{
      key: "initialise",
      value: function initialise() {}
    }]);

    return Dialogue;
  })();

  module.exports = Dialogue;
});