define(["exports", "module", "jquery", "utilities/classManipulator"], function (exports, module, _jquery, _utilitiesClassManipulator) {
  "use strict";

  var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();

  function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

  var _ClassManipulator = _interopRequireDefault(_utilitiesClassManipulator);

  /**
  * breadcrumbsEnhanced.js - forces the breadcrumbs onto one line using various truncation methods
  *
  * @since 0.1.14
  *
  * @author Digital Services <communications@det.nsw.edu.au>
  * @copyright © 2016 State Government of NSW 2016
  *
  * @class
  * @requires jQuery
  */

  var BreadcrumbsEnhanced = (function () {

    /**
    * Creates a new BreadcrumbsEnhanced
    *
    * @constructor
    *
    * @param {String|Element|jQuery} selector - Either a CSS selector, DOM element or matched jQuery object
    * @param {Object} config - class configuration options. Options vary depending on need
    * @param {Int} config.items_threshold - amount of breadcrumb items before attempting to truncate
    * @param {Int} config.items_visible - amount of visible breadcrumb items after truncating
    * @param {String|Element|jQuery} config.selectors.list - the list of breadcrumbs
    * @param {String|Element|jQuery} config.selectors.item - the breadcrumb items
    * @param {String|Element|jQuery} config.selectors.dropdown - the generated dropdown element
    * @param {String} config.classes.item - class name of the breadcrumb items
    * @param {String} config.classes.truncate - class name of the truncated items
    * @param {String} config.classes.dropdown - class name of the generate dropdown
    *
    */

    function BreadcrumbsEnhanced(selector, config) {
      _classCallCheck(this, BreadcrumbsEnhanced);

      this.config = {
        items_threshold: 5,
        items_visible: 3,
        selectors: {
          list: ".gef-breadcrumbs__list",
          item: ".gef-breadcrumbs__item",
          dropdown: ".gef-breadcrumbs__dropdown"
        },
        classes: {
          item: "gef-breadcrumbs__item",
          truncate: "gef-breadcrumbs__truncate",
          dropdown: "gef-breadcrumbs__dropdown"
        }
      };

      // Check if config has been passed to constructor
      if (config) {
        // Merge default config with passed config
        this.config = _jquery.extend(true, {}, this.config, config);
      }

      var self = this,
          $list = (0, _jquery)(selector).find(this.config.selectors.list),
          totalWidth = parseInt($list.width()),
          // width of breadcrumbs container
      $items = $list.find(this.config.selectors.item),
          length = $items.length,
          threshold = this.config.items_threshold,
          // amount of items allowed before adding dropdown
      visibleItems = this.config.items_visible,
          // amount of items to always show after dropdown
      divisor = 100,
          // percentage of available width
      dropdownWidth = 0,
          // width of the dropdown item
      collectWidths = 0,
          // combined actual width of items without truncation
      itemWidth = divisor / length; // decimal percentage width per item

      // Assign necessary things to class object
      this.$list = $list;
      this.totalWidth = totalWidth;
      this.divisor = divisor;
      this.firstLoop = true;
      this.selector = selector;
      this.collectWidths = collectWidths;

      if (length > threshold) {
        // add the dropdown
        var dropdown = '<li class="' + this.config.classes.item + ' ' + this.config.classes.dropdown + '"><div></div></li>';
        $items.eq(0).after(dropdown);

        // add some mouse interaction utilities to the dropdown
        var $dropdown = $items.eq(0).next(),
            dropDownClick = new _ClassManipulator["default"]($dropdown, {
          class_name: "active-clicked",
          event_type: "click"
        }),
            dropDownHover = new _ClassManipulator["default"]($dropdown, {
          class_name: "active-hovered",
          event_type: "mouseout",
          click_outside: false,
          timer: {
            on: true,
            delay: 1000
          }
        });

        // add items which are not the first or the visible items to the dropdown
        $items.each(function (i) {
          if (i > 0 && i < length - visibleItems) {
            (0, _jquery)(this).appendTo($list.find(self.config.selectors.dropdown + ' div'));
          }
        });

        // Convert the dropdown items to p tags for accessibility reasons
        var $dropdownItems = $dropdown.find("li");
        $dropdownItems.each(function () {
          var html = (0, _jquery)(this).html();
          (0, _jquery)(this).replaceWith("<p>" + html + "</p>");
        });

        // Show the dropdown if it's children are focused
        var dropDownFocus = new _ClassManipulator["default"]($dropdown.find('a'), {
          class_name: "active-focused",
          event_type: "focus blur",
          toggle: true,
          $subject: $dropdown,
          click_outside: true
        });

        // update $items to just the visible items and define their max width
        $items = $list.find('li').not(this.config.selectors.dropdown);
        length = $items.length;
        dropdownWidth = Math.floor(parseInt((0, _jquery)(this.config.selectors.dropdown).outerWidth()) / totalWidth * 100);
        this.divisor = this.divisor - dropdownWidth; // update the divisor to not include the dropdown item
        itemWidth = this.divisor / length;
      }

      // Screen resize
      (0, _jquery)(window).resize(function () {
        return self.resetItems(itemWidth, $items);
      });

      // Truncate the visible items
      this.truncateItems(itemWidth, $items);
    }

    /*
    * Loops through items and tries to give them an optimal width based on space available
    *
    * @truncateItems
    *
    * @param {int} itemWidth - the available width in percentage
    * @param {array} itemsArr - array of items to test the width with
    */

    _createClass(BreadcrumbsEnhanced, [{
      key: "truncateItems",
      value: function truncateItems(itemWidth, itemsArr) {

        var totalWidthPx = this.totalWidth * this.divisor / 100,
            // amount of available total width in px
        itemWidthPx = Math.floor(itemWidth * totalWidthPx / 100),
            // amount of available width for each item in px
        count = 0,
            // amount of times an item's actual width is less than its available width
        self = this;

        // Add the activated class to the breadcrumbs
        (0, _jquery)(this.selector).addClass('activated');

        itemsArr.each(function (i) {

          var width = (0, _jquery)(this).find('> a, > span').outerWidth(),
              // get actual item width
          text = (0, _jquery)(this).find('> a, > span').text(),
              item = this;

          // Add the tooltip content
          (0, _jquery)(this).attr("data-tooltip", text);

          // if the item's actual width is less than the available width
          if (width < itemWidthPx) {
            self.collectWidths += width;
            count++;
            // remove the tooltip and truncation
            (0, _jquery)(item).removeClass(self.config.classes.truncate);

            // remove the item from the looping array
            itemsArr = itemsArr.filter(function (v) {
              return v != itemsArr.index(item);
            });
          } else {
            // If the actual width is longer than the available width, add the tooltip and truncation
            (0, _jquery)(item).addClass(self.config.classes.truncate);
          }
        });

        var length = itemsArr.length;
        // If there's been a change && if there's still items in the array || first time running ? then loop through again to make use of available width
        if (count > 0 && length > 0 || this.firstLoop) {
          var leftOver = this.divisor - this.collectWidths / totalWidthPx * 100,
              // get the leftover width
          split = Math.floor(leftOver / length); // find the available remaining width for each item in the array
          itemsArr.css('max-width', split - 0.5 + "%"); // set the max-width
          this.firstLoop = false;

          // do it again!
          this.truncateItems(split, itemsArr);
        } else {
          //do nothing
          return;
        }
      }

      /*
      * Resets all the items to not have the truncate class or a max width then calls truncateItems again
      *
      * @resetItems
      *
      * @param {int} itemWidth - the available width in percentage
      * @param {array} itemsArr - array of items to test the width with
      */
    }, {
      key: "resetItems",
      value: function resetItems(itemWidth, $items) {
        var self = this;
        this.firstLoop = true;
        // Remove any styling
        (0, _jquery)(this.selector).removeClass('activated');
        $items.attr('style', '').removeClass(this.config.classes.truncate);
        // Reset the width
        this.totalWidth = parseInt(this.$list.width());
        this.collectWidths = 0;
        // reset the timeout
        clearTimeout(this.timeout);
        // Truncate items after 500ms
        this.timeout = setTimeout(function () {
          self.truncateItems(itemWidth, $items);
        }, 100);
      }
    }]);

    return BreadcrumbsEnhanced;
  })();

  module.exports = BreadcrumbsEnhanced;
});