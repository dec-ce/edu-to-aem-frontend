define(["exports", "module", "placeholder/jquery.placeholder", "jquery"], function (exports, module, _placeholderJqueryPlaceholder, _jquery) {
  "use strict";

  function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

  var _placeholder = _interopRequireDefault(_placeholderJqueryPlaceholder);

  var IE9Placeholder = function IE9Placeholder(selector, config) {
    _classCallCheck(this, IE9Placeholder);

    (0, _jquery)('input, textarea').placeholder();
  };

  module.exports = IE9Placeholder;
});