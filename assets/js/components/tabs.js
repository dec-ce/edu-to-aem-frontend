define(["exports", "module", "jquery", "utilities/ariaToggler"], function (exports, module, _jquery, _utilitiesAriaToggler) {
  "use strict";

  var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();

  function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

  var _AriaToggler = _interopRequireDefault(_utilitiesAriaToggler);

  /**
   * Tabs.js manages the necessary changes in DOM structure between the mobile and tablet breakpoints.
   *
   * @since 1.0.0
   *
   * @author Digital Services <communications@det.nsw.edu.au>
   * @copyright © 2015 State Government of NSW 2015
   *
   * @class
   * @requires jQuery
   */

  var Tabs = (function () {
    function Tabs(selector, config) {
      _classCallCheck(this, Tabs);

      /**
       *
       * @param {string} config.breakpoint: the point at which the tabs change from accordions (mobile) to tabs (desktop)
       * @param {string} config.selectors.anchors: the links which activate the tabs.
       * @param {string} config.selectors.panels: the content sections which move in the DOM to create the tabs layout
       * @param {string} config.classes.active: the class to add to the selector to show js is active
       * @param {string} config.elements.tabletGrouping: the element(s) which wrap the content sections on desktop layout
       *
       */
      this.config = {
        breakpoint: "screen and (min-width: 768px)",
        selectors: {
          anchors: "[role=tab]",
          panels: "[role=tabpanel]"
        },
        classes: {
          active: "gef-tabs--active"
        },
        elements: {
          tabletGrouping: "<div class=\"gef-tabs__panel-group\"></div>"
        }
      };

      // Instantiate the toggler
      // this.toggle = new AriaToggler(selector)

      // // Check if config has been passed to constructor
      if (config) {
        // Merge default config with passed config
        this.config = _jquery.extend(true, {}, this.config, config);
      }

      var self = this;
      function checkBreakpoint(mql) {
        console.log("function");
        if (mql.matches) {
          // Tablet and up
          self.changeToDesktop();
        } else {
          // Mobile and up
          self.changeToMobile();
          // Scroll the screen to match the focus box on Voice Over.
          // self.$container.find('.gef-tabs__group > a').on('mouseup', function() {
          //   var $link = $(this)
          //   setTimeout(function() {
          //     $('html, body').animate({
          //       scrollTop: $link.offset().top
          //     }, 200)
          //   }, 500)
          // })
        }
      }

      // Check if selector has been passed to constructor
      if (selector) {
        // Use jQuery to match find the relevant DOM element(s)
        this.$container = (0, _jquery)(selector);
        // add active class to container
        this.$container.addClass(this.config.classes.active);
        // jQuery obj for tablet panel grouping
        this.$tabletGrouping = (0, _jquery)(this.config.elements.tabletGrouping);
        // jQuery obj for panels
        this.$panels = this.$container.find(this.config.selectors.panels);
        this.$anchors = this.$container.find(this.config.selectors.anchors);
        // set the breakpoint matcher
        if (window.matchMedia === undefined) {
          // on ie9 just change to desktop version
          this.changeToDesktop();
        } else {
          this.match = window.matchMedia(this.config.breakpoint);
          // test the breakpoint at runtime
          checkBreakpoint(this.match);
          // listen for breakpoint changes
          this.match.addListener(checkBreakpoint);
        }
      }
    }

    _createClass(Tabs, [{
      key: "changeToDesktop",
      value: function changeToDesktop() {

        // wrap the panels in list items for semantics
        this.$panels.wrap("<div></div>");
        // Append to the tablet container
        this.$container.append(this.$tabletGrouping.append((0, _jquery)(this.$panels.parent())));
        // Focus selection for accessibility
        // Set array of of anchors and panels to variables
        var panels = this.$panels,
            anchors = this.$anchors;
        // Loop through anchors
        this.$anchors.each(function (i) {
          // // On click/keyboard/touch
          // $(this).on("click", function() {
          //   // Remove tabindex attr from all panels
          //   panels.each(function() {
          //     $(this).removeAttr("tabindex")
          //   })
          //   // For relating panel add it to the tab index, focus and setup the focus out event
          //   $(panels[i]).attr("tabindex", "0").focus().on("focusout", function() {
          //     // If this isn't the last tab panel
          //     if(i <= anchors.length - 2) {
          //       // Set the relating anchor to focus
          //       $(anchors[i]).focus()
          //     } else {
          //       // Otherwise business as usual
          //       return true
          //     }
          //   })
          // }).keyup(function(e) {
          //   if(e.keyCode === 13) {
          //     $(this).click()
          //   }
          // })
        });
      }
    }, {
      key: "changeToMobile",
      value: function changeToMobile() {
        var _this = this;

        // Test to see if the tabs have been modified for desktop
        if (this.$container.find('.gef-tabs__panel-group').length) {
          (function () {

            var $anchors = _this.$container.find(_this.config.selectors.anchors),
                _self = _this;

            // loop through anchors and add the panels after them
            $anchors.each(function (i) {
              $anchors.eq(i).after(self.$panels.eq(i));
            });

            // delete .get-tabs__panel-group
            _this.$container.find('.gef-tabs__panel-group').remove();
          })();
        } else {
          // do nothing
        }
      }
    }]);

    return Tabs;
  })();

  module.exports = Tabs;
});