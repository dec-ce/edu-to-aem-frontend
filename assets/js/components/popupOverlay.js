define(["exports", "module", "jquery", "utilities/classManipulator"], function (exports, module, _jquery, _utilitiesClassManipulator) {
  "use strict";

  var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();

  function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

  var _classManipulator = _interopRequireDefault(_utilitiesClassManipulator);

  /**
  * popupOverlay.js - This component creates a popup window with a dimmed background. Leverages the classManipulator utility.
  *
  * @since 0.1.09
  *
  * @author Digital Services <communications@det.nsw.edu.au>
  * @copyright © 2016 State Government of NSW 2016
  *
  * @class
  * @requires jQuery
  */

  var PopupOverlay = (function () {

    //     @param {string} config.selector  - The selector that triggers popup window
    //     @param {string} config.event_type    - Type of event that triggers the popup window
    //     @param {string} config.class_name - A class to add to the popup window
    //     @param {string} config.subject     - The class of the div that will be the popup window
    //     @param {object} config.toggle - Toggles the popup window.
    //     @param {string} config.click_outside   - If you want the popup window to close when clicking outside the window.
    //     @param {string} config.insertBeforeElement    - Where in the DOM the background div element will be created.
    //     @param {string} config.policy-popup-background  -  The class that will be attached to the dimmed background.

    function PopupOverlay(selector, config) {
      _classCallCheck(this, PopupOverlay);

      // Set the selector
      this.$object = (0, _jquery)(selector);

      // Instantiate the classManipulator
      this.popupWindow = new _classManipulator["default"](selector, config);

      //Element where the popup div will be inserted before
      var insertBefore = config.insertBeforeElement;

      //Class of the popup background
      var backgroundClass = config.backgroundClass;

      //Class of the popup window
      var popupClass = config.class_name;

      //Class of the object you want to add popup window to
      var popupSubject = config.subject;

      //Create a new div
      var popupObjectPosition = document.createElement("div");
      //Add a class to the new div create so we can locate it later
      popupObjectPosition.className = 'popupObjectPosition';

      //This stores data for elements that 'aria-hidden' is applied to.
      var bodyChildren = [];

      //Load background
      this.popupBackground(insertBefore, backgroundClass, popupClass, popupSubject, popupObjectPosition, bodyChildren);
    }

    _createClass(PopupOverlay, [{
      key: "popupBackground",
      value: function popupBackground(insertBefore, backgroundClass, popupClass, popupSubject, popupObjectPosition, bodyChildren) {
        var toggleState = 'active';

        this.$object.click(function () {

          if (toggleState == 'active') {

            // Give all non-modal top level elements an ARIA-hidden tag    
            (0, _jquery)("body").children().each(function () {
              var jQchild = (0, _jquery)(this);

              var o = {};
              o.jqel = jQchild;

              if (jQchild.attr("aria-hidden") !== undefined) {
                o.hadVal = true;
                o.originalVal = jQchild.attr("aria-hidden");
              } else {
                o.hadVal = false;
              }
              bodyChildren.push(o);
              //hide the child
              jQchild.attr("aria-hidden", "true");
            });

            //Show dimmed background
            (0, _jquery)('.atlwdg-blanket').show();
            (0, _jquery)('.atlwdg-blanket').css('z-index', 12);

            //Add popup class to the object that is to become a popup
            (0, _jquery)(popupSubject).addClass(popupClass);

            //Place the new div next to the popup div
            (0, _jquery)(popupSubject).after(popupObjectPosition);

            //The target position of where we are going to move the popup elements
            var targetPosition = (0, _jquery)("div").last();

            // Move popup elements, we move this to the bottom of the page for accessibility tabbing.
            targetPosition.after((0, _jquery)(popupSubject));

            //Focus on the top element when popup open
            var a = document.querySelector('.policy-enquires-main-content');
            a.getElementsByTagName('h2')[0].focus();

            (0, _jquery)('.policy-enquires-main-content h2').on('focus', function () {
              (0, _jquery)(this).bind('keydown', function (e) {
                if (e.shiftKey && e.keyCode == 9) {
                  return false;
                }
              });
            });

            toggleState = 'inactive';
          }
        });

        //Save 'this' object so can be used within functions
        var _this = this;

        //Close popup when close icon button is clicked
        (0, _jquery)('.close-popup-icon').bind('click', function (e) {
          if (toggleState == 'inactive') {
            if (e.keyCode == 13) {
              _this.removePopup(popupClass, popupSubject, bodyChildren);
              toggleState = 'active';
            }
            _this.removePopup(popupClass, popupSubject, bodyChildren);
            toggleState = 'active';
          }
        });

        //Return focus to first element of popup after tabbing from close button
        (0, _jquery)('.close-popup-icon').on('focus', function () {
          (0, _jquery)(this).bind('keydown', function (e) {

            // Prevent tab key going beyond close button, but still allowing shift + tab
            if (e.keyCode == 9) {
              if (e.shiftKey) {
                return true;
              } else {
                return false;
              }
            }
          });
        });

        //Close popup when escape key is clicked
        (0, _jquery)(document).keyup(function (e) {
          if (toggleState == 'inactive') {
            if (e.keyCode == 27) {
              // escape key maps to keycode `27`
              _this.removePopup(popupClass, popupSubject, bodyChildren);
              toggleState = 'active';
            }
          }
        });
      }
    }, {
      key: "removePopup",
      value: function removePopup(popupClass, popupSubject, bodyChildren) {

        //Remove the background div element & popupbox
        (0, _jquery)('.atlwdg-blanket').hide();
        (0, _jquery)("." + popupClass).removeClass(popupClass);

        //Remove Popup Class
        (0, _jquery)(popupSubject).removeClass(popupClass);

        //Remove popupObjectPosition element
        (0, _jquery)('.popupObjectPosition').after((0, _jquery)(popupSubject));
        (0, _jquery)('.popupObjectPosition').remove();

        //Focus on element once window closed
        this.$object.focus();

        //Remove ARIA tags that were previously added by script
        for (var i = 0, c; c = bodyChildren[i]; i++) {
          //if element originally had aria-hidden attribute, reinstate value
          if (c.hadVal) {
            c.jqel.attr("aria-hidden", c.originalVal);
            //if no original aria-hidden, remove the attribute entirely
          } else {
              c.jqel.removeAttr("aria-hidden");
            }
        }
      }
    }]);

    return PopupOverlay;
  })();

  module.exports = PopupOverlay;
});